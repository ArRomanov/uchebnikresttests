package JsonClasses.AuthMethod;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type",
    "roles",
    "user_id",
    "agree_pers_data",
    "school_id",
    "school_shortname",
    "subject_ids"
})
public class Profile {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    @NotNull
    private Integer id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    @NotNull
    private String type;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("roles")
    @Valid
    @NotNull
    private List<Object> roles = new ArrayList<Object>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    @NotNull
    private Integer userId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("agree_pers_data")
    @NotNull
    private Boolean agreePersData;
    @JsonProperty("school_id")
    private Integer schoolId;
    @JsonProperty("school_shortname")
    private String schoolShortname;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subject_ids")
    @Valid
    @NotNull
    private List<Object> subjectIds = new ArrayList<Object>();
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("roles")
    public List<Object> getRoles() {
        return roles;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("roles")
    public void setRoles(List<Object> roles) {
        this.roles = roles;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    public Integer getUserId() {
        return userId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("user_id")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("agree_pers_data")
    public Boolean getAgreePersData() {
        return agreePersData;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("agree_pers_data")
    public void setAgreePersData(Boolean agreePersData) {
        this.agreePersData = agreePersData;
    }

    @JsonProperty("school_id")
    public Integer getSchoolId() {
        return schoolId;
    }

    @JsonProperty("school_id")
    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    @JsonProperty("school_shortname")
    public String getSchoolShortname() {
        return schoolShortname;
    }

    @JsonProperty("school_shortname")
    public void setSchoolShortname(String schoolShortname) {
        this.schoolShortname = schoolShortname;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subject_ids")
    public List<Object> getSubjectIds() {
        return subjectIds;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subject_ids")
    public void setSubjectIds(List<Object> subjectIds) {
        this.subjectIds = subjectIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(type).append(roles).append(userId).append(agreePersData).append(schoolId).append(schoolShortname).append(subjectIds).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Profile) == false) {
            return false;
        }
        Profile rhs = ((Profile) other);
        return new EqualsBuilder().append(id, rhs.id).append(type, rhs.type).append(roles, rhs.roles).append(userId, rhs.userId).append(agreePersData, rhs.agreePersData).append(schoolId, rhs.schoolId).append(schoolShortname, rhs.schoolShortname).append(subjectIds, rhs.subjectIds).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
