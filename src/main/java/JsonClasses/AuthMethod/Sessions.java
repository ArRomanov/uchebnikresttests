package JsonClasses.AuthMethod;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "email",
    "profiles",
    "first_name",
    "middle_name",
    "last_name",
    "phone_number",
    "authentication_token",
    "password_change_required"
})
public class Sessions {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    @NotNull
    private Integer id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("email")
    @NotNull
    private String email;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("profiles")
    @Valid
    @NotNull
    private List<Profile> profiles = new ArrayList<Profile>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("first_name")
    @NotNull
    private String firstName;
    @JsonProperty("middle_name")
    private String middleName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("last_name")
    @NotNull
    private String lastName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("phone_number")
    @NotNull
    private String phoneNumber;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("authentication_token")
    @NotNull
    private String authenticationToken;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password_change_required")
    @NotNull
    private Boolean passwordChangeRequired;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("profiles")
    public List<Profile> getProfiles() {
        return profiles;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("profiles")
    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("middle_name")
    public String getMiddleName() {
        return middleName;
    }

    @JsonProperty("middle_name")
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("phone_number")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("authentication_token")
    public String getAuthenticationToken() {
        return authenticationToken;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("authentication_token")
    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password_change_required")
    public Boolean getPasswordChangeRequired() {
        return passwordChangeRequired;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password_change_required")
    public void setPasswordChangeRequired(Boolean passwordChangeRequired) {
        this.passwordChangeRequired = passwordChangeRequired;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(email).append(profiles).append(firstName).append(middleName).append(lastName).append(phoneNumber).append(authenticationToken).append(passwordChangeRequired).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Sessions) == false) {
            return false;
        }
        Sessions rhs = ((Sessions) other);
        return new EqualsBuilder().append(id, rhs.id).append(email, rhs.email).append(profiles, rhs.profiles).append(firstName, rhs.firstName).append(middleName, rhs.middleName).append(lastName, rhs.lastName).append(phoneNumber, rhs.phoneNumber).append(authenticationToken, rhs.authenticationToken).append(passwordChangeRequired, rhs.passwordChangeRequired).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
