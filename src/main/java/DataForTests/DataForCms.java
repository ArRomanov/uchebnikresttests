package DataForTests;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataForCms {

    public static int newAssignment;
    public static int newAssignmentBookItem;

    public static String getCurrentDateYearMmDd() {
        long curentDate = new Date().getTime();
        return new SimpleDateFormat("yyyy-MM-dd").format(curentDate);
    }

    public static String getCurrentTimeHhMmSs(){
        long curentDate = new Date().getTime();
        return new SimpleDateFormat("HH:mm:ss").format(curentDate);
    }
}
