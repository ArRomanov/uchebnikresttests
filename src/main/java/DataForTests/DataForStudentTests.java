package DataForTests;

import DataBase.queries.ForTestGroupsAndVariants;

import java.util.Map;

import static DataBase.queries.ForTestSpecs.getTaskNumAndTaskTypeOfVariant;

public class DataForStudentTests {

    public static int newVariant;
    public static int newGroup;
    public static String specId;
    public static int CountOfQuestionsInTest;

    public static Object[][] getNumAndTypeOfTasksInVariant() {
        String variantFromDB = ForTestGroupsAndVariants.getLastGeneratedVariant();
        newVariant = Integer.parseInt(variantFromDB);
        Map<String, String> numAndTypeOfTask = getTaskNumAndTaskTypeOfVariant();
        Object[][] arrayOfTaskNumAndType = new Object[numAndTypeOfTask.size()][2];
        int counter = 0;
        for (Map.Entry entry : numAndTypeOfTask.entrySet()) {
            arrayOfTaskNumAndType[counter][0] = entry.getKey();
            arrayOfTaskNumAndType[counter][1] = entry.getValue();
            counter++;
        }
        return arrayOfTaskNumAndType;
    }

}
