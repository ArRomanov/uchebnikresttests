package DataForTests;

public class PathsAndIds {
    public static final String STABLE_BASE_URL = "";

    public static final String API_SESSION = "";
    public static final String API_SUBJECTS = "";
    public static final String API_USERS = "";
    public static final String API_EQUIPMENT = "";
    public static final String API_GENRES = "";
    public static final String API_LICENSE_TYPE = "";

    //exam/rest/secure
    public static final String EXAM_SECURE = "";
    public static final String EXAM_USER_INFO = "";
    public static final String EXAM_TASK = "";
    public static final String EXAM_SPEC = "";
    public static final String EXAM_QUOTA = "";
    public static final String EXAM_EDUCATION_LEVELS = "";
    public static final String EXAM_STUDYING_LEVELS = "";
    public static final String EXAM_FOLDERS = "";
    public static final String EXAM_ANSWER_TYPES = "";
    public static final String EXAM_SUBJECTS = "";
    public static final String TESTPLAYER_GROUP = "";
    public static final String TEST_PLAYER_VARIANT = "";
    public static final String USER_VARIANT = "";
    public static final String TRAINING_TEST = "";
    public static final String BINDING = "";
    public static final String BINDING_MODERATION_LESSON = "";
    public static final String VARIANTS_GENERATE = "";
    public static final String VARIANTS = "";
    public static final String VARIANTS_OF_TEST_TASK = "";
    public static final String ANSWER = "";
    public static final String API_SPECIFICATION = "";
    public static final String TASK_OF_VARIANT = "";
    public static final String GROUP_VARIANT = "";

    //exam-tdesk/rest/secure
    public static final String TDESK_ACADEMIC_YEAR = "";
    public static final String TDESK_ACTIVITY_HISTORY_MATERIAL = "";
    public static final String TDESK_ACTIVITY_MATERIAL = "";
    public static final String TDESK_ACTIVITY_CLASSES = "";
    public static final String TDESK_VARIANT = "";
    public static final String TDESK_VARIANT_GROUP = "";
    public static final String TDESK_USER_VARIANT = "";
    public static final String TDESK_ANSWERS_VARIANT = "";
    public static final String TDESK_TASK_VARIANT = "";

    //cms
    public static final String CMS_API = "";
    public static final String MATERIALS_URL = "";
    public static final String ALL_FOLDERS_URL = "";
    public static final String TEST_SPECIFICATIONS = "";
    public static final String BOOK_TYPES = "";
    public static final String PUBLISHER_ACTIVATION_CODE = "";
    public static final String LOGICAL_TYPES = "";
    public static final String LESSON_TEMPLATES = "";
    public static final String MATERIAL_COUNT_BY_SUBJECTS = "";
    public static final String CMS_BOOKS = "";
    public static final String CMS_ASSIGNMENT_BOOK_ITEMS = "";
    public static final String CMS_ASSIGNMENT = "";
    public static final String CMS_ATOMIC_OBJECT = "";
    public static final String CMS_BOOK_VERSION = "";
    public static final String CMS_COMPOSED_DOCUMENT = "";
    public static final String CMS_DIGITAL_DOCUMENT = "";
    public static final String CMS_COUNT_OF_MATERIALS = "";

    public static final String MODERATION = "";
    public static final String MODERATION_HISTORY = "";

    //local
    public static final String PATH_TO_JSON_BODIES = "src/test/resources/";

    //identifications
    public static final String ROOT_FOLDER = "";
    public static final String FAKE_STUDYING_LEVEL_ID = "";
    public static final String COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE = "";
    public static final String EDUCATION_LEVEL_ID_HARD_CODE = "";
    public static final String SUBJECT_ID_HARD_CODE = "";
    public static final String STUDYING_LEVEL_ID_HARD_CODE = "";
    public static final String LOGICAL_TYPE_ID_HARD_CODE = "";
    public static final String CONTROLLABLE_ITEM_ID_FOR_CMS_ONLY = "";
    public static final String SUBJECT_ID_FOR_CMS_ONLY = "";
}
