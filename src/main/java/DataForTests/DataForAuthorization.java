package DataForTests;

import Helpers.ForAuthorization;

import java.util.HashMap;
import java.util.Map;

public class DataForAuthorization {

    //Data for auth (login/password)
    public static final String TEACHER_LOGIN = "";
    public static final String TEACHER_PASS = "";
    public static final String STUDENT_LOGIN = "";
    public static final String STUDENT_PASS = "";
    public static final String METHODOLOGIST_LOGIN = "";
    public static final String METHODOLOGIST_PASS = "";

    //roles
    public static int userIdTeacher;
    public static int profileIdTeacher;
    public static String authTokenTeacher;

    public static int userIdStudent;
    public static int profileIdStudent;
    public static String authTokenStudent;

    public static int userIdAuthor;
    public static int profileIdAuthor;
    public static String authTokenAuthor;

    public static int userIdReader;
    public static int profileIdReader;
    public static String authTokenReader;

    public static int userIdMethodologist;
    public static int profileIdMethodologist;
    public static String authTokenMethodologist;

    public static int userIdPublisher;
    public static int profileIdPublisher;
    public static String authTokenPublisher;

    private static Map<String, String> cookiesForTeacher = new HashMap<>();
    private static Map<String, String> cookiesForStudent = new HashMap<>();
    private static Map<String, String> cookiesForMethodologist = new HashMap<>();
    private static Map<String, String> cookiesForPublisher = new HashMap<>();

    private static Map<String, String> createMapOfCookiesForRole(int userId, int profileId, String authToken) {
        Map<String, String> cookies = new HashMap<>();
        cookies.put("auth_token", authToken);
        cookies.put("user_id", Integer.toString(userId));
        cookies.put("profile_id", Integer.toString(profileId));
        return cookies;
    }

    public static Map<String, String> getCookiesForTeacher() {
        if (cookiesForTeacher.isEmpty()) {
            ForAuthorization.getAuthorization(TEACHER_LOGIN, TEACHER_PASS);
            cookiesForTeacher = createMapOfCookiesForRole(userIdTeacher, profileIdTeacher, authTokenTeacher);
        }
        return cookiesForTeacher;
    }

    public static Map<String, String> getCookiesForStudent() {
        if (cookiesForStudent.isEmpty()) {
            ForAuthorization.getAuthorization(STUDENT_LOGIN, STUDENT_PASS);
            cookiesForStudent = createMapOfCookiesForRole(userIdStudent, profileIdStudent, authTokenStudent);
        }
        return cookiesForStudent;
    }

    public static Map<String, String> getCookiesForMethodologist() {
        if (cookiesForMethodologist.isEmpty()) {
            ForAuthorization.getAuthorization(METHODOLOGIST_LOGIN, METHODOLOGIST_PASS);
            cookiesForMethodologist = createMapOfCookiesForRole(userIdMethodologist, profileIdMethodologist, authTokenMethodologist);
        }
        return cookiesForMethodologist;
    }

    public static Map<String, String> getCookiesForPublisher() {
        if (cookiesForPublisher.isEmpty()) {
            ForAuthorization.getAuthorization(TEACHER_LOGIN, TEACHER_PASS);
            cookiesForPublisher = createMapOfCookiesForRole(userIdPublisher, profileIdPublisher, authTokenPublisher);
        }
        return cookiesForPublisher;
    }

}
