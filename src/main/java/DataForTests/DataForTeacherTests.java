package DataForTests;

import DataBase.queries.ForTestGroupsAndVariants;
import ResourcesClasses.JsonBodiesForTestTasks;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DataForTeacherTests {
    private static List<String> listOfTestTask = new ArrayList<>();
    public static int newFolderId;
    public static int newTestSpec;
    public static int newLessonTemplate;
    public static int newBindingId;
    public static int groupOfGeneratedVariants;
    public static int newVariant;
    public static int newBook;
    public static int newComposedDocuments;
    public static int newDigitalDocuments;

    public static Object[][] getLevelsForSubjects() {
        Object[][] levelsOfEducation = {{"1"}, {"2"}, {"3"}, {"4"}, {"5"}};
        return levelsOfEducation;
    }

    public static Object[][] getJsonBodiesForCreateTestTask() throws InvocationTargetException, IllegalAccessException {
        Class classObjectWithJsonBodies = JsonBodiesForTestTasks.class;
        Method[] arrayOfGetBodiesMethods = classObjectWithJsonBodies.getDeclaredMethods();
        int countOfGetBodyMethods = arrayOfGetBodiesMethods.length;
        Object[][] jsonBodies = new Object[countOfGetBodyMethods][1];
        String methodName;
        Method currentMethod;
        for (int i = 0; i < countOfGetBodyMethods; i++) {
            currentMethod = arrayOfGetBodiesMethods[i];
            methodName = currentMethod.getName();
            Object resultOfGetMethod = currentMethod.invoke(methodName, new Object[]{});
            jsonBodies[i][0] = (String) resultOfGetMethod;

        }

        return jsonBodies;
    }

    public static void addTestTask(int testTask) {
        listOfTestTask.add(Integer.toString(testTask));
    }

    public static Object[][] getIdsTaskOfVariant() {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        List<String> idsFromBD = ForTestGroupsAndVariants.getTaskIdOfVariant(variantId);
        Object[][] objects = new Object[idsFromBD.size()][1];
        for (int counter = 0; counter < idsFromBD.size(); counter++) {
            objects[counter][0] = idsFromBD.get(counter);
        }
        return objects;
    }
}
