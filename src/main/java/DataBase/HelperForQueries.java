package DataBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelperForQueries {
    private static Connection connToStable;


    public static List<String> executeSelectGetListOfRows(String query) {
        List<String> result = new ArrayList<>();
        try {
            connToStable = Connector.getConnection();
            Statement statement = connToStable.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            while (!rs.isAfterLast()) {
                result.add(rs.getString(1));
                rs.next();
            }
        } catch (SQLException e) {
            System.out.println("\n Problems with executing SELECT");
        }
        return result;
    }

    public static Map<String, String> executeSelectGetPairCol1Col2(String query) {
        Map<String, String> result = new HashMap<>();
        try {
            connToStable = Connector.getConnection();
            Statement statement = connToStable.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            while (!rs.isAfterLast()) {
                result.put(rs.getString(1), rs.getString(2));
                rs.next();
            }
        } catch (SQLException e) {
            System.out.println("\n Problems with executing SELECT");
        }
        return result;
    }

    public static List<String> executeSelectGetListOfColums(String query, int countOfColumns) {
        List<String> result = new ArrayList<>();
        try {
            connToStable = Connector.getConnection();
            Statement statement = connToStable.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            for (int counter = 1; counter <= countOfColumns; counter++) {
                result.add(rs.getString(counter));
            }
        } catch (SQLException e) {
            System.out.println("\n Problems with executing SELECT");
        }
        return result;
    }

    public static boolean executeSelectGetBoolean(String query) throws SQLException {
        connToStable = Connector.getConnection();
        Statement statement = connToStable.createStatement();
        ResultSet rs = statement.executeQuery(query);
        rs.next();
        return rs.getBoolean(1);
    }

    public static void executeUpdate(String query) throws SQLException {
        connToStable = Connector.getConnection();
        Statement statement = connToStable.createStatement();
        statement.executeUpdate(query);
    }

    public static void executeDelete(String query) throws SQLException {
        try {
            connToStable = Connector.getConnection();
            Statement statement = connToStable.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
            connToStable.close();
        }
    }
}
