package DataBase.queries;

import DataBase.HelperForQueries;

import java.util.List;

public class Other {
    public static List<String> getQuotaInfo(String userId) {
        String query = "SELECT quota_limit,quota_current\n" +
                "FROM public.user_quotas\n" +
                "where user_id=" + userId;
        return HelperForQueries.executeSelectGetListOfColums(query, 2);
    }
}
