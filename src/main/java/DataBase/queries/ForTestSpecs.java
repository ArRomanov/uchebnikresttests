package DataBase.queries;

import DataBase.HelperForQueries;
import Helpers.Converters;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForAuthorization.userIdTeacher;
import static DataForTests.DataForStudentTests.newVariant;

public class ForTestSpecs {

    public static String getLastTestSpec() {
        String query = "select id from test_specifications\n" +
                "where deleted_at is null\n" +
                "order by 1 desc\n" +
                "limit 1;";
        System.out.println("\n...getting last test spec...\n");
        String specId = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return specId;
    }

    public static String getLastTestSpecByTeacher(String teacherId) {
        String query = "SELECT ts.id\n" +
                "FROM test_specifications ts\n" +
                "join folders_materials fm\n" +
                "on ts.id=fm.material_id\n" +
                "where ts.\"name\" = 'autoSpec'\n" +
                "and ts.user_id=" + teacherId + "\n" +
                "and fm.material_type='TestSpecification'\n" +
                "order by 1 desc;";
        String specId = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return specId;
    }

    public static String getModerationStatusOfSpec(int specId) {
        String query = "select moderation_status from public.test_specifications\n" +
                "where id=" + specId;
        String status = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return status;
    }

    public static String getLastSpecForPassing() {
        String query = "select id from test_specifications\n" +
                "where \"name\" = 'autoSpec'\n" +
                "and moderation_status=3\n" +
                "and user_id=" + userIdTeacher + "\n" +
                "order by 1 desc\n" +
                "limit 1;";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static Map<String, String> getRightAnswerForTestSpec() {
        Map<String, String> numAndRightAnswers = getTaskNumAndRightAnswerInVariant();
        Map<String, String> numAndTaskType = getTaskNumAndTaskTypeOfVariant();
        Map<String, String> finalBody = generatingPairsTaskNumAndJsonForRequest(numAndRightAnswers, numAndTaskType);
        return finalBody;
    }

    private static Map<String, String> getTaskNumAndRightAnswerInVariant() {
        String query = "select num, (select * from json_extract_path_text(task_content,'answer','right_answer'))\n" +
                "FROM public.generated_variants_tasks\n" +
                "where variant_id=" + newVariant + "\n" +
                "order by num;";
        Map<String, String> numAndRightAnswers = HelperForQueries.executeSelectGetPairCol1Col2(query);
        return numAndRightAnswers;
    }

    public static Map<String, String> getTaskNumAndTaskTypeOfVariant() {
        String query = "SELECT num,  (select * from json_extract_path_text(task_content,'answer','type'))\n" +
                "FROM public.generated_variants_tasks\n" +
                "where variant_id=" + newVariant + "\n" +
                "order by num;";
        Map<String, String> numAndTaskType = HelperForQueries.executeSelectGetPairCol1Col2(query);
        return numAndTaskType;
    }

    private static Map<String, String> generatingPairsTaskNumAndJsonForRequest(Map<String, String> numAndRightAnswers, Map<String, String> numAndTaskType) {
        Map<String, String> numAndRightAnswer = new HashMap<>();
        JSONObject rightAnswerJson;
        JSONObject finalBodyForRequestAnswer = new JSONObject();
        Object rightAnswerObj;
        Object typeOfTask;
        String numOfTask;
        for (Map.Entry entry : numAndRightAnswers.entrySet()) {
            rightAnswerObj = entry.getValue();
            typeOfTask = numAndTaskType.get(entry.getKey());
            numOfTask = entry.getKey().toString();
            rightAnswerJson = parsingNullValues(rightAnswerObj, typeOfTask);
            rightAnswerJson.put("@answer_type", typeOfTask);
            finalBodyForRequestAnswer.put("answer", rightAnswerJson);
            numAndRightAnswer.put(numOfTask, finalBodyForRequestAnswer.toString());
        }
        return numAndRightAnswer;
    }

    private static JSONObject parsingNullValues(Object rightAnswerObj, Object typeOfTask) {
        JSONObject rightAnswerJson = new JSONObject();
        try {
            String rightAnswerStr = rightAnswerObj.toString();
            return Converters.parseStringToJson(rightAnswerStr);
        } catch (NullPointerException e) {
            if (typeOfTask.equals("answer/table")) {
                rightAnswerJson.put("cells", new JSONObject());
            } else {
                rightAnswerJson.put("string", "");
            }
            return rightAnswerJson;
        }
    }
}
