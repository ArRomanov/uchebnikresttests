package DataBase.queries;

import DataBase.HelperForQueries;

import java.sql.SQLException;
import java.util.List;

import static DataForTests.DataForAuthorization.userIdTeacher;
import static DataForTests.DataForStudentTests.newVariant;

public class ForTestGroupsAndVariants {

    public static String getLastGeneratedVariant() {
        String query = "SELECT id\n" +
                "FROM public.generated_variants\n" +
                "order by 1 desc;";
        List<String> result = HelperForQueries.executeSelectGetListOfRows(query);
        return result.get(0);
    }

    public static String getStatusOfTest() {
        String query = "SELECT status\n" +
                "FROM public.users_bind_variants\n" +
                "where variant_id=" + newVariant;
        String status = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return status;
    }

    public static String getLastGeneratedTestGroup() {
        String query = "select generated_variants_id\n" +
                "FROM public.generated_variants_bind_groups\n" +
                "order by 1 desc\n" +
                "limit 1";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getLastFinishedVariantForSpecByCurrentTeacher() {
        String query = "SELECT ttua.variant_id\n" +
                "FROM public.test_tasks_users_answers ttua\n" +
                "join public.users_bind_variants ubv\n" +
                "on ttua.variant_id=ubv.variant_id\n" +
                "where ubv.status='stop'\n" +
                "and ttua.variant_id in (\n" +
                "select variant_id\n" +
                "FROM public.generated_variants_bind_groups\n" +
                "where generated_variants_id in (\n" +
                "select id from generated_variants_groups\n" +
                "where test_specification_id in(\n" +
                "select id from test_specifications\n" +
                "where user_id=" + userIdTeacher + "\n" +
                ")))\n" +
                "order by ttua.variant_id desc\n" +
                "limit 1";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static List<String> getTaskIdOfVariant(String variantId) {
        String query = "SELECT task_id\n" +
                "FROM public.generated_variants_tasks\n" +
                "where variant_id=" + variantId;
        return HelperForQueries.executeSelectGetListOfRows(query);
    }

    public static String getUserIdByVariant(String variantId) {
        String query = "SELECT user_id\n" +
                "FROM public.test_tasks_users_answers\n" +
                "where variant_id=\n" + variantId +
                "limit 1";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static boolean isUsersBindWithVariants(int variantId, int userId) {
        String query = "SELECT id,variant_id,user_id\n" +
                "FROM public.users_bind_variants\n" +
                "where variant_id=" + variantId + "\n" +
                "and user_id=" + userId;
        List<String> binding = HelperForQueries.executeSelectGetListOfColums(query, 3);
        if (binding.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean getRaitingOfTask(String variantId, String taskId) throws SQLException {
        String query = "select is_right from public.test_tasks_users_answers\n" +
                "where variant_id=" + variantId + "\n" +
                "and task_id=" + taskId;
        return HelperForQueries.executeSelectGetBoolean(query);
    }

    public static String getGroupIdByVariant(int variantId) {
        String query = "select generated_variants_id\n" +
                "FROM public.generated_variants_bind_groups\n" +
                "where variant_id=" + variantId;
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static List<String> getVariantsByGroupId(int groupId) {
        String query = "select variant_id\n" +
                "FROM public.generated_variants_bind_groups\n" +
                "where generated_variants_id=" + groupId;
        return HelperForQueries.executeSelectGetListOfRows(query);
    }

    public static String getStatusOfTestVariants(String variantId) {
        String query = "SELECT status\n" +
                "FROM public.users_bind_variants\n" +
                "where variant_id=" + variantId;
        String status = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return status;
    }

    public static int getCountOfStudentAnswers() {
        String query = "SELECT count(id)\n" +
                "FROM public.test_tasks_users_answers\n" +
                "where variant_id=" + newVariant;
        String countOfStudentAnswers = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return Integer.parseInt(countOfStudentAnswers);
    }
}
