package DataBase.queries;

import DataBase.HelperForQueries;

import java.util.List;

import static DataForTests.DataForAuthorization.userIdStudent;

public class ForAtomicObjects {

    public static String getLastAtomicObject() {
        String atomicId;
        String query = "select id from public.atomic_objects\n" +
                "order by 1 desc\n" +
                "limit 1";
        atomicId = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return atomicId;
    }

    public static String getLastTestTask() {
        String query = "select fm.material_id\n" +
                "from folders_materials fm\n" +
                "join atomic_objects ao\n" +
                "on fm.material_id=ao.id\n" +
                "where fm.material_type='AtomicObject'\n" +
                "and ao.logical_type_id between 26 and 34\n" +
                "order by 1 desc\n" +
                "limit 1;";
        System.out.println("\n...getting last test task...\n");
        String taskId = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return taskId;
    }

    public static List<String> getTaskWithOpenAnswer() {
        String query = "select user_id, variant_id,task_id from public.test_tasks_users_answers\n" +
                "where variant_id in (\n" +
                "select variant_id from generated_variants_bind_groups\n" +
                "where generated_variants_id in(\n" +
                "select id from generated_variants_groups\n" +
                "where user_id=" + userIdStudent + "\n" +
                "))\n" +
                "and is_right is null\n" +
                "order by task_id desc\n" +
                "limit 1;";
        return HelperForQueries.executeSelectGetListOfColums(query, 3);
    }


    public static String getModerationStatusOfTestTask(int testTaskId) {
        String query = "SELECT moderation_status\n" +
                "FROM public.atomic_objects\n" +
                "where id=" + testTaskId;
        String status = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return status;
    }
}
