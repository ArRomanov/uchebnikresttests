package DataBase.queries;

import DataBase.HelperForQueries;

import java.sql.SQLException;

public class ForCms {

    public static String getLastBookVersionId() {
        String query = "SELECT id\n" +
                "FROM public.book_versions\n" +
                "order by 1 desc\n" +
                "limit 1";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getLastNotDeletedBook() {
        String query = "select * from books\n" +
                "where deleted_at is null\n" +
                "order by 1 desc";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getbookIdByVersionId(String bookVersionId) {
        String query = "SELECT book_id\n" +
                "FROM public.book_versions\n" +
                "where id=" + bookVersionId + "\n" +
                "order by 1 desc";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getLastAssignmentBook() {
        String query = "select id\n" +
                "from assignment_book_items\n" +
                "order by 1 desc\n" +
                "limit 1";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getAssignmentIdIdByAssigmentItemId(String assigmentId) {
        String query = "select assignment_id\n" +
                "from assignment_book_items\n" +
                "where id = " + assigmentId + "\n" +
                "order by 1 desc";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getLastAssignment() {
        String query = "select id\n" +
                "from assignments\n" +
                "order by 1 desc";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static void updSpecDateCreationForTest(String date, int specId) throws SQLException {     //sample date format "2017-09-08 14:36:58"
        String queryForUpdate = "update test_specifications\n" +
                "set created_at='" + date + "'\n" +
                "where id=" + specId;
        HelperForQueries.executeUpdate(queryForUpdate);
    }

    public static int getCountOfAtomicObj() {
        String query = "select count(id) from atomic_objects\n" +
                "where deleted_at is null;";
        String countOfElementStr = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return Integer.parseInt(countOfElementStr);
    }

    public static String getLastNotDeletedComposedDocument() {
        String query = "select id from composed_documents\n" +
                "where deleted_at is null\n" +
                "order by 1 desc\n" +
                "limit 1;";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }

    public static String getLastNotDeletedDigitalDocs(){
        String query = "select id from digital_documents\n" +
                "order by 1 desc\n" +
                "limit 1;";
        return HelperForQueries.executeSelectGetListOfRows(query).get(0);
    }
}
