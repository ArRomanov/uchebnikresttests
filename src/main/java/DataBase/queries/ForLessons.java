package DataBase.queries;

import DataBase.HelperForQueries;

public class ForLessons {

    public static String getModerationStatusOfLesson(int lessonTemplate){
        String query = "select moderation_status \n" +
                "from lesson_templates\n" +
                "where id=" + lessonTemplate;
        String status = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return status;
    }

    public static String getLastNotDeletedLessonTemplates() {
        String query = "select id \n" +
                "from lesson_templates\n" +
                "where deleted_at is null\n" +
                "order by 1 desc\n" +
                "limit 1;";
        String lastLessonTemplate = HelperForQueries.executeSelectGetListOfRows(query).get(0);
        return lastLessonTemplate;
    }
}
