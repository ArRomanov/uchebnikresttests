package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private static final String DB_URL = "";
    private static final String USER = "";
    private static final String PASS = "";
    protected static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
                createConnection();
        }
        return connection;
    }

    private static void createConnection() {
        try {
            connection = DriverManager
                    .getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            System.out.println("Connector Failed");
            e.printStackTrace();
        }
    }

    public static void closeConnection() throws SQLException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Problem with closing DB connection");
            e.printStackTrace();
        }
    }
}

