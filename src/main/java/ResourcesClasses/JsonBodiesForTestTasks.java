package ResourcesClasses;

import static DataForTests.PathsAndIds.*;

public class JsonBodiesForTestTasks {

    public static String getBodyForTaskWithFreeAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"дайте открытый ответ\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/free\",\n" +
                "      \"options\": [],\n" +
                "      \"right_answer\": null\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"openAnswer\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithMultipleAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"выберете 1 и 2 вариант\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/multiple\",\n" +
                "      \"options\": [\n" +
                "        {\n" +
                "          \"id\": \"5h7td8\",\n" +
                "          \"text\": \"1 вариант\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"6qvb4a\",\n" +
                "          \"text\": \"2 вариант\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"4edvji\",\n" +
                "          \"text\": \"3 вариант\",\n" +
                "          \"content\": []\n" +
                "        }\n" +
                "      ],\n" +
                "      \"right_answer\": {\n" +
                "        \"ids\": [\n" +
                "          \"5h7td8\",\n" +
                "          \"6qvb4a\"\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"multiple\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithMatchAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"установите соответствие\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/match\",\n" +
                "      \"options\": [\n" +
                "        {\n" +
                "          \"id\": \"238qo2\",\n" +
                "          \"text\": \"a\",\n" +
                "          \"type\": \"option_type/match/source\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"e8l7e7\",\n" +
                "          \"text\": \"1\",\n" +
                "          \"type\": \"option_type/match/target\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"27vh80\",\n" +
                "          \"text\": \"b\",\n" +
                "          \"type\": \"option_type/match/source\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"mu0hal\",\n" +
                "          \"text\": \"2\",\n" +
                "          \"type\": \"option_type/match/target\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"43nmho\",\n" +
                "          \"text\": \"c\",\n" +
                "          \"type\": \"option_type/match/source\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"pqvb72\",\n" +
                "          \"text\": \"3\",\n" +
                "          \"type\": \"option_type/match/target\",\n" +
                "          \"content\": []\n" +
                "        }\n" +
                "      ],\n" +
                "      \"right_answer\": {\n" +
                "        \"match\": {\n" +
                "          \"238qo2\": [\n" +
                "            \"e8l7e7\"\n" +
                "          ],\n" +
                "          \"27vh80\": [\n" +
                "            \"mu0hal\"\n" +
                "          ],\n" +
                "          \"43nmho\": [\n" +
                "            \"pqvb72\"\n" +
                "          ]\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"setAccordance\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": {\n" +
                "      \"name\": \"RomanovAV\",\n" +
                "      \"source\": \"link\"\n" +
                "    },\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithNumberAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"введите число 1\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/number\",\n" +
                "      \"options\": [],\n" +
                "      \"right_answer\": {\n" +
                "        \"number\": 1\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"inputInteger\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithOrderAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"расставьте по порядку\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/order\",\n" +
                "      \"options\": [\n" +
                "        {\n" +
                "          \"id\": \"uesm9m\",\n" +
                "          \"text\": \"2\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"7k4tnt\",\n" +
                "          \"text\": \"4\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"61rt91\",\n" +
                "          \"text\": \"1\",\n" +
                "          \"content\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\": \"lk2gjl\",\n" +
                "          \"text\": \"3\",\n" +
                "          \"content\": []\n" +
                "        }\n" +
                "      ],\n" +
                "      \"right_answer\": {\n" +
                "        \"ids_order\": [\n" +
                "          \"61rt91\",\n" +
                "          \"uesm9m\",\n" +
                "          \"lk2gjl\",\n" +
                "          \"7k4tnt\"\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"inOrder\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithSingleAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"?\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/single\",\n" +
                "      \"options\":[\n" +
                "        {\n" +
                "          \"id\":\"Right\",\n" +
                "          \"text\":\"!\",\n" +
                "          \"content\":[]\n" +
                "        },\n" +
                "        {\n" +
                "          \"id\":\"notRight\",\n" +
                "          \"text\":\"?\",\n" +
                "          \"content\":[]\n" +
                "        }\n" +
                "      ],\n" +
                "      \"right_answer\":{\"id\":\"Right\"}\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"OneRightAnswer\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithStringAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"введите строку\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/string\",\n" +
                "      \"options\": [],\n" +
                "      \"right_answer\": {\n" +
                "        \"string\": \"строка\",\n" +
                "        \"caseSensitive\": false\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"InputString\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyForTaskWithTableAnswer() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"заполните таблицу\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/table\",\n" +
                "      \"options\": [\n" +
                "        {\n" +
                "          \"id\": \"vipfmo\",\n" +
                "          \"text\": \"\",\n" +
                "          \"type\": \"option_type/table\",\n" +
                "          \"content\": [\n" +
                "            {\n" +
                "              \"type\": \"content/table\",\n" +
                "              \"table\": {\n" +
                "                \"cells\": {\n" +
                "                  \"0\": {\n" +
                "                    \"1\": \"1 столбец\",\n" +
                "                    \"2\": \"2 столбец\",\n" +
                "                    \"3\": \"3 столбец\"\n" +
                "                  },\n" +
                "                  \"1\": {\n" +
                "                    \"0\": \"1 строка\",\n" +
                "                    \"2\": \"2\",\n" +
                "                    \"3\": \"3\"\n" +
                "                  },\n" +
                "                  \"2\": {\n" +
                "                    \"0\": \"2 строка\",\n" +
                "                    \"1\": \"4\",\n" +
                "                    \"3\": \"6\"\n" +
                "                  },\n" +
                "                  \"3\": {\n" +
                "                    \"0\": \"3 строка\",\n" +
                "                    \"1\": \"7\",\n" +
                "                    \"2\": \"8\"\n" +
                "                  }\n" +
                "                },\n" +
                "                \"rows\": 4,\n" +
                "                \"columns\": 4,\n" +
                "                \"rowHeaders\": true,\n" +
                "                \"columnHeaders\": true\n" +
                "              }\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      ],\n" +
                "      \"right_answer\": null\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"заполнение таблицы\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }

    public static String getBodyOfTaskForCms() {
        return "{\n" +
                "  \"test_task\": {\n" +
                "    \"id\": null,\n" +
                "    \"question_elements\": [\n" +
                "      {\n" +
                "        \"type\": \"content/text\",\n" +
                "        \"text\": \"Фильтрация по КЭС\",\n" +
                "        \"content\": []\n" +
                "      }\n" +
                "    ],\n" +
                "    \"answer\": {\n" +
                "      \"type\": \"answer/free\",\n" +
                "      \"options\": [],\n" +
                "      \"right_answer\": null\n" +
                "    }\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"name\": \"уникальный КЭС id\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_FOR_CMS_ONLY + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "        \"controllable_items\": [\n" +
                "          {\n" +
                "            \"id\": " + CONTROLLABLE_ITEM_ID_FOR_CMS_ONLY + ",\n" +
                "            \"name\": \"null\",\n" +
                "            \"code\": \"null\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  }\n" +
                "}";
    }
}
