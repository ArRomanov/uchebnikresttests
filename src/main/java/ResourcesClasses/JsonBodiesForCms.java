package ResourcesClasses;

import static DataForTests.PathsAndIds.*;

public class JsonBodiesForCms {
    public static String getBodyForChangeAssignment() {
        return "{\n" +
                "  \"activity_type_id\": 32,\n" +
                "  \"lesson_stage_id\": 39000,\n" +
                "  \"name\": \"AutotestAssigmentChanged\",\n" +
                "  \"text\": \"TestSpecification\"\n" +
                "}";
    }

    public static String getBodyForChangeAssignmentBookItem() {
        return "{\n" +
                "  \"assignment_id\":9036,\n" +
                "  \"material_type\":\"Book\",\n" +
                "  \"ebook_id\":\"0000004E\",\n" +
                "  \"ebook_version\":\"0.25\",\n" +
                "  \"path\":\"3.0.2\",\n" +
                "  \"title\":\"AutotestItemChanged\",\n" +
                "  \"item_type\":\"theory\"\n" +
                "}";
    }

    public static String getBodyForNewAssignment() {
        return "{\n" +
                "  \"activity_type_id\": 32,\n" +
                "  \"lesson_stage_id\": 39000,\n" +
                "  \"name\": \"AutotestAssigment\",\n" +
                "  \"text\": \"TestSpecification\"\n" +
                "}";
    }

    public static String getBodyForNewAssignmentBookItem() {
        return "{\n" +
                "  \"assignment_id\": 9036,\n" +
                "  \"material_type\": \"Book\",\n" +
                "  \"ebook_id\": \"0000004E\",\n" +
                "  \"ebook_version\": \"0.25\",\n" +
                "  \"path\": \"3.0.2\",\n" +
                "  \"title\": \"AutotestItem\",\n" +
                "  \"item_type\": \"theory\"\n" +
                "}";
    }

    public static String getBodyForNewBook() {
        return "{\n" +
                "\"ebook_id\":\"0AAAAAAA\",\n" +
                "\"title\":\"autoCreateBook\",\n" +
                "\"authors\":\"Romanov\",\n" +
                "\"book_type_id\":2,\n" +
                "\"publish_year\":2017,\n" +
                "\"publisher_name\":\"Romanoff\"\n" +
                "}";
    }

    public static String getBodyForTextAtomic() {
        return "{\n" +
                "  \"content_type\": \"text\",\n" +
                "  \"material_education_areas_attributes\": [\n" +
                "    {\n" +
                "      \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "      \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + "\n" +
                "    }\n" +
                "  ],\n" +
                "  \"controllable_item_ids\": [\n" +
                "    " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + "\n" +
                "  ],\n" +
                "  \"time_to_study\": \"00:11:11\",\n" +
                "  \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "  \"logical_type_id\": 3,\n" +
                "  \"caption\": null,\n" +
                "  \"description\": \"Автоматически созданный тестовый атомик\",\n" +
                "  \"folder_ids\": [\n" +
                "    " + ROOT_FOLDER + "\n" +
                "  ],\n" +
                "  \"published\": true,\n" +
                "  \"author\": \"\",\n" +
                "  \"author_url\": \"\"\n" +
                "}";
    }
}
