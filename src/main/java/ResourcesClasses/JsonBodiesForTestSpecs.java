package ResourcesClasses;

import static DataForTests.PathsAndIds.*;

public class JsonBodiesForTestSpecs {

    public static String getBodyForCreatingSpec() {
        return "{\n" +
                "  \"id\": null,\n" +
                "  \"basic_info\": {\n" +
                "    \"name\": \"autoSpec\",\n" +
                "    \"description\": \"this specification created automatly\",\n" +
                "    \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "    \"subjects\": [\n" +
                "      {\n" +
                "        \"subject_id\": " + SUBJECT_ID_HARD_CODE + ",\n" +
                "        \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + "\n" +
                "      }\n" +
                "    ],\n" +
                "    \"author\": null,\n" +
                "    \"folder_id\": " + ROOT_FOLDER + "\n" +
                "  },\n" +
                "  \"test_parameters\": {\n" +
                "    \"test_groups_count\": 1,\n" +
                "    \"test_duration\": 10,\n" +
                "    \"test_variants_count\": 1,\n" +
                "    \"testing_order\": \"seriatim\",\n" +
                "    \"test_auto_creation\": true,\n" +
                "    \"allow_variants_generation_to_everyone\": true\n" +
                "  },\n" +
                "  \"test_groups\": [\n" +
                "    {\n" +
                "      \"id\": null,\n" +
                "      \"name\": \"Блок с названием\",\n" +
                "      \"weight\": 100,\n" +
                "      \"questions_per_variant_count\": 9,\n" +
                "      \"controllable_items\": [],\n" +
                "      \"tasks\": [\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"заполните таблицу цифрами по порядку\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/table\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"vipfmo\",\n" +
                "                  \"text\": \"\",\n" +
                "                  \"type\": \"option_type/table\",\n" +
                "                  \"content\": [\n" +
                "                    {\n" +
                "                      \"type\": \"content/table\",\n" +
                "                      \"table\": {\n" +
                "                        \"cells\": {\n" +
                "                          \"0\": {\n" +
                "                            \"1\": \"1 столбец\",\n" +
                "                            \"2\": \"2 столбец\",\n" +
                "                            \"3\": \"3 столбец\"\n" +
                "                          },\n" +
                "                          \"1\": {\n" +
                "                            \"0\": \"1 строка\",\n" +
                "                            \"2\": \"2\",\n" +
                "                            \"3\": \"3\"\n" +
                "                          },\n" +
                "                          \"2\": {\n" +
                "                            \"0\": \"2 строка\",\n" +
                "                            \"1\": \"4\",\n" +
                "                            \"3\": \"6\"\n" +
                "                          },\n" +
                "                          \"3\": {\n" +
                "                            \"0\": \"3 строка\",\n" +
                "                            \"1\": \"7\",\n" +
                "                            \"2\": \"8\"\n" +
                "                          }\n" +
                "                        },\n" +
                "                        \"rows\": 4,\n" +
                "                        \"columns\": 4,\n" +
                "                        \"rowHeaders\": true,\n" +
                "                        \"columnHeaders\": true\n" +
                "                      }\n" +
                "                    }\n" +
                "                  ]\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": null\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"заполните таблицу цифрами по порядку\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"введите строку\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/string\",\n" +
                "              \"options\": [],\n" +
                "              \"right_answer\": {\n" +
                "                \"string\": \"строка\",\n" +
                "                \"caseSensitive\": false\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"введите строку\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"?\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/single\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"hwh6dn\",\n" +
                "                  \"text\": \"!\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"cr914d\",\n" +
                "                  \"text\": \"?\",\n" +
                "                  \"content\": []\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": {\n" +
                "                \"id\": \"hwh6dn\"\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"?\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"расставьте по порядку\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/order\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"0bkjw2\",\n" +
                "                  \"text\": \"2\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"8jxcok\",\n" +
                "                  \"text\": \"4\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"c5sbna\",\n" +
                "                  \"text\": \"1\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"64aijk\",\n" +
                "                  \"text\": \"3\",\n" +
                "                  \"content\": []\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": {\n" +
                "                \"ids_order\": [\n" +
                "                  \"c5sbna\",\n" +
                "                  \"0bkjw2\",\n" +
                "                  \"64aijk\",\n" +
                "                  \"8jxcok\"\n" +
                "                ]\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"расставьте по порядку\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"введите число 1\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/number\",\n" +
                "              \"options\": [],\n" +
                "              \"right_answer\": {\n" +
                "                \"number\": 1\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"введите число 1\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"выберете 1 и 2 вариант\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/multiple\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"r1r9wf\",\n" +
                "                  \"text\": \"1 вариант\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"ikhn42\",\n" +
                "                  \"text\": \"2 вариант\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"7w667k\",\n" +
                "                  \"text\": \"3 вариант\",\n" +
                "                  \"content\": []\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": {\n" +
                "                \"ids\": [\n" +
                "                  \"r1r9wf\",\n" +
                "                  \"ikhn42\"\n" +
                "                ]\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"выберете 1 и 2 вариант\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"установите соответствие\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/match\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"kb6dtv\",\n" +
                "                  \"text\": \"a\",\n" +
                "                  \"type\": \"option_type/match/source\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"7rl1be\",\n" +
                "                  \"text\": \"1\",\n" +
                "                  \"type\": \"option_type/match/target\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"x4trfn\",\n" +
                "                  \"text\": \"b\",\n" +
                "                  \"type\": \"option_type/match/source\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"og941b\",\n" +
                "                  \"text\": \"2\",\n" +
                "                  \"type\": \"option_type/match/target\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"rgd9se\",\n" +
                "                  \"text\": \"c\",\n" +
                "                  \"type\": \"option_type/match/source\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"grxc97\",\n" +
                "                  \"text\": \"3\",\n" +
                "                  \"type\": \"option_type/match/target\",\n" +
                "                  \"content\": []\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": {\n" +
                "                \"match\": {\n" +
                "                  \"kb6dtv\": [\n" +
                "                    \"7rl1be\"\n" +
                "                  ],\n" +
                "                  \"x4trfn\": [\n" +
                "                    \"og941b\"\n" +
                "                  ],\n" +
                "                  \"rgd9se\": [\n" +
                "                    \"grxc97\"\n" +
                "                  ]\n" +
                "                }\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"установите соответствие\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": {\n" +
                "              \"name\": \"RomanovAV\",\n" +
                "              \"source\": \"link\"\n" +
                "            },\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"распределите по группам\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/groups\",\n" +
                "              \"options\": [\n" +
                "                {\n" +
                "                  \"id\": \"smcopv\",\n" +
                "                  \"text\": \"группа 1\",\n" +
                "                  \"type\": \"option_type/group\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"4qo0s2\",\n" +
                "                  \"text\": \"ответ 1 для группы 1\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"oqdb2a\",\n" +
                "                  \"text\": \"ответ 2 для группы 1\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"rnksin\",\n" +
                "                  \"text\": \"группа 2\",\n" +
                "                  \"type\": \"option_type/group\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"o5lnmx\",\n" +
                "                  \"text\": \"ответ 1 для группы 2\",\n" +
                "                  \"content\": []\n" +
                "                },\n" +
                "                {\n" +
                "                  \"id\": \"wksabf\",\n" +
                "                  \"text\": \"ответ 2 для группы 2\",\n" +
                "                  \"content\": []\n" +
                "                }\n" +
                "              ],\n" +
                "              \"right_answer\": {\n" +
                "                \"groups\": [\n" +
                "                  {\n" +
                "                    \"group_id\": \"smcopv\",\n" +
                "                    \"options_ids\": [\n" +
                "                      \"4qo0s2\",\n" +
                "                      \"oqdb2a\"\n" +
                "                    ]\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"group_id\": \"rnksin\",\n" +
                "                    \"options_ids\": [\n" +
                "                      \"o5lnmx\",\n" +
                "                      \"wksabf\"\n" +
                "                    ]\n" +
                "                  }\n" +
                "                ]\n" +
                "              }\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"распределите по группам\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        },\n" +
                "        {\n" +
                "          \"test_task\": {\n" +
                "            \"id\": null,\n" +
                "            \"question_elements\": [\n" +
                "              {\n" +
                "                \"type\": \"content/text\",\n" +
                "                \"text\": \"дайте открытый ответ\",\n" +
                "                \"content\": []\n" +
                "              }\n" +
                "            ],\n" +
                "            \"answer\": {\n" +
                "              \"type\": \"answer/free\",\n" +
                "              \"options\": [],\n" +
                "              \"right_answer\": null\n" +
                "            }\n" +
                "          },\n" +
                "          \"meta\": {\n" +
                "            \"name\": \"дайте открытый ответ\",\n" +
                "            \"studying_level_id\": null,\n" +
                "            \"subjects\": [],\n" +
                "            \"author\": null,\n" +
                "            \"folder_id\": null\n" +
                "          }\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }
}
