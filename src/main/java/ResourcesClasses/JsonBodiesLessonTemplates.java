package ResourcesClasses;

import static DataForTests.PathsAndIds.*;

public class JsonBodiesLessonTemplates {

    public static String getBodyForCreateLessonTemplate() {
        return "{\n" +
                "  \"url\": \"lesson_templates\",\n" +
                "  \"build_with_composer\": true,\n" +
                "  \"material_type\": \"lesson\",\n" +
                "  \"forDelete\": [],\n" +
                "  \"materialCounter\": 0,\n" +
                "  \"equipments\": [\n" +
                "    {\n" +
                "      \"id\": 1,\n" +
                "      \"ratio\": 0.5625\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 3,\n" +
                "      \"ratio\": 0.75\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 4,\n" +
                "      \"ratio\": 0.75\n" +
                "    }\n" +
                "  ],\n" +
                "  \"material_education_areas_attributes\": [\n" +
                "    {\n" +
                "      \"education_level_id\": " + EDUCATION_LEVEL_ID_HARD_CODE + ",\n" +
                "      \"subject_id\": " + SUBJECT_ID_HARD_CODE + "\n" +
                "    }\n" +
                "  ],\n" +
                "  \"name\": \"autoLesson\",\n" +
                "  \"topic_name\": \"The theme\",\n" +
                "  \"description\": \"Discription\",\n" +
                "  \"studying_level_id\": " + STUDYING_LEVEL_ID_HARD_CODE + ",\n" +
                "  \"folder_ids\": [\n" +
                ROOT_FOLDER +
                "  ],\n" +
                "  \"controllable_item_ids\": [\n" +
                "    " + COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE + "\n" +
                "  ],\n" +
                "  \"json_content\": \"{\\\"used_materials\\\":[],\\\"stages\\\":[],\\\"description\\\":\\\"Discription\\\",\\\"name\\\":\\\"autoLesson\\\",\\\"topic_name\\\":\\\"The theme\\\"}\"\n" +
                "}";
    }
}
