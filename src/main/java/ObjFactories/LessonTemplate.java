package ObjFactories;

import DataBase.HelperForQueries;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static DataBase.queries.ForLessons.getLastNotDeletedLessonTemplates;
import static DataForTests.PathsAndIds.*;
import static ResourcesClasses.JsonBodiesLessonTemplates.getBodyForCreateLessonTemplate;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class LessonTemplate extends ObjectForTest {
    LessonTemplate() {
        super();
    }

    public int createNew() {
        System.out.println("\n...creating of lesson template...\n");
        int idLessonTemplate = given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .body(getBodyForCreateLessonTemplate())
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + LESSON_TEMPLATES)
                .then().statusCode(201)
                .extract().jsonPath().get("id");
        System.out.println("\n...lesson template created: " + idLessonTemplate + "...\n");
        return idLessonTemplate;
    }

    private List<String> getBindingIdByLessonId(int lessonId) {
        String query = "select id from test_materials_versions_usages\n" +
                "where (select * from  json_extract_path_text(usage_info,'bound_to_entity_id'))='" + lessonId + "';";
        return HelperForQueries.executeSelectGetListOfRows(query);
    }

    private void deleteBindingsByLessons(int lessonId) {
        List<String> listOfBindId = getBindingIdByLessonId(lessonId);
        for (int counter = 0; counter < listOfBindId.size(); counter++) {
            given()
                    .cookies(mapOfCookiesTeacher)
                    .contentType(JSON)
                    .header("Accept", "*/*")
                    .log().method().log().uri().log().cookies()
                    .when().delete(STABLE_BASE_URL + BINDING + "/" + listOfBindId.get(counter))
                    .then().statusCode(200);
        }
    }

    public void deleteObj(int materialId) throws SQLException {
        deleteBindingsByLessons(materialId);
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + materialId)
                .then().statusCode(204);
        System.out.println("\n...lesson template " + materialId + " deleted...\n");
    }

    public void toModerationStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("published", true);
        body.put("moderation_status", "for_approval");
        System.out.println("\n...sending of lesson template to moderation...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + materialId)
                .then().statusCode(200);
        body.replace("moderation_status", "accepted");
        given()
                .cookies(mapOfCookiesMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + materialId)
                .then().statusCode(200);
        System.out.println("\n...moderation accepted for lesson " + materialId + "...\n");
    }

    public void toNewStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("published", true);
        body.put("moderation_status", "new");
        System.out.println("\n...to 'new' moderation status for lesson template...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + materialId)
                .then().statusCode(200);
    }

    public int getLastFromDb() {
        String lastLessonId = getLastNotDeletedLessonTemplates();
        return Integer.parseInt(lastLessonId);
    }

    public void updModerationStatus(int materialId, int newStatusId) throws SQLException {
        String query = "update lesson_templates\n" +
                "set moderation_status=" + newStatusId + "\n" +
                "where id=" + materialId;
        HelperForQueries.executeUpdate(query);
    }
}
