package ObjFactories;

import DataBase.HelperForQueries;
import DataBase.queries.ForTestSpecs;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForTeacherTests.newTestSpec;
import static DataForTests.PathsAndIds.*;
import static ResourcesClasses.JsonBodiesForTestSpecs.getBodyForCreatingSpec;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class TestSpec extends ObjectForTest {
    TestSpec() {
        super();
    }

    public int createNew() {
        System.out.println("\n...creating of test specifications...\n");
        int idCreatedSpec = given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForCreatingSpec())
                .when().post(STABLE_BASE_URL + EXAM_SPEC + "/")
                .then().statusCode(200)
                .extract().jsonPath().get("id");
        System.out.println("\n...test spec created: " + idCreatedSpec + "...\n");
        return idCreatedSpec;
    }

    public void deleteObj(int materialId) throws SQLException {
        System.out.println("\n...deleting test spec...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .header("Accept", "application/json")
                .when().delete(STABLE_BASE_URL + EXAM_SPEC + "/" + materialId)
                .then().statusCode(200);
        System.out.println("\n...test spec " + materialId + " deleted...\n");
    }

    public void toModerationStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "pending");
        body.put("message", null);
        System.out.println("\n...sending of test specifications to moderation...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + materialId + MODERATION)
                .then().statusCode(200);
        body.replace("moderation_status", "accepted");
        given()
                .cookies(mapOfCookiesMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + materialId + MODERATION)
                .then().statusCode(200);
        System.out.println("\n...moderation accepted for spec " + materialId + "...\n");
    }

    public void toNewStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "new");
        body.put("message", null);
        System.out.println("\n...to 'new' moderation status for test spec...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + newTestSpec + MODERATION)
                .then().statusCode(200);
        System.out.println("\n...test spec " + materialId + " is in status 'new'...\n");
    }

    public int getLastFromDb() {
        String specId = ForTestSpecs.getLastTestSpec();
        return Integer.parseInt(specId);
    }

    public void updModerationStatus(int materialId, int newStatusId) throws SQLException {
        String query = "update test_specifications\n" +
                "set moderation_status=" + newStatusId + "\n" +
                "where id=" + materialId;
        HelperForQueries.executeUpdate(query);
    }
}
