package ObjFactories;

public class Factories {
    private static ObjectForTest atomicFactory;
    private static ObjectForTest lessonFactory;
    private static ObjectForTest specFactory;
    private static ObjectForTest taskFactory;

    public static ObjectForTest getAtomicFactory() {
        if (atomicFactory == null) {
            atomicFactory = new Atomic();
        }
        return atomicFactory;
    }

    public static ObjectForTest getLessonFactory() {
        if (lessonFactory == null) {
            lessonFactory = new LessonTemplate();
        }
        return lessonFactory;
    }

    public static ObjectForTest getSpecFactory() {
        if (specFactory == null) {
            specFactory = new TestSpec();
        }
        return specFactory;
    }

    public static ObjectForTest getTaskFactory() {
        if (taskFactory == null) {
            taskFactory = new TestTask();
        }
        return taskFactory;
    }
}
