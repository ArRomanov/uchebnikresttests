package ObjFactories;

import DataBase.HelperForQueries;
import DataBase.queries.ForAtomicObjects;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.PathsAndIds.*;
import static ResourcesClasses.JsonBodiesForCms.getBodyForTextAtomic;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class Atomic extends ObjectForTest {
    Atomic() {
        super();
    }

    public int createNew() {
        System.out.println("\n...creating of atomic object...\n");
        int idCreatedAtomic = given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForTextAtomic())
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().statusCode(200)
                .extract().path("id");
        System.out.println("\nAtomic object created: " + idCreatedAtomic);
        return idCreatedAtomic;
    }

    public void deleteObj(int materialId) throws SQLException {
        System.out.println("\n...deleting atomic...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .when().delete(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + materialId)
                .then().statusCode(204);
        System.out.println("\n...Atomic " + materialId + " deleted...\n");
    }

    public void toModerationStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("published", true);
        body.put("moderation_status", "for_approval");
        System.out.println("\n...sending of atomic to moderation...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForTextAtomic())
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + materialId)
                .then().statusCode(200);
        body.replace("moderation_status", "accepted");
        given()
                .cookies(mapOfCookiesMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + materialId)
                .then().statusCode(200);
        System.out.println("\n...moderation accepted for atomic " + materialId + "...\n");
    }

    public void toNewStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("published", true);
        body.put("moderation_status", "new");
        System.out.println("\n...to 'new' moderation status for atomic object...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + materialId)
                .then().statusCode(200);
    }

    public int getLastFromDb() {
        String lastAtomicId = ForAtomicObjects.getLastAtomicObject();
        return Integer.parseInt(lastAtomicId);
    }

    public void updModerationStatus(int materialId, int newStatusId) throws SQLException {
        String query = "update atomic_objects\n" +
                "set moderation_status=" + newStatusId + "\n" +
                "where id=" + materialId;
        HelperForQueries.executeUpdate(query);
    }
}
