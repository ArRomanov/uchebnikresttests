package ObjFactories;

import DataForTests.DataForAuthorization;

import java.sql.SQLException;
import java.util.Map;

public abstract class ObjectForTest {
    Map<String, String> mapOfCookiesTeacher;
    Map<String, String> mapOfCookiesStudent;
    Map<String, String> mapOfCookiesMethodist;
    Map<String, String> mapOfCookiesPublisher;

    ObjectForTest() {
        this.mapOfCookiesTeacher = DataForAuthorization.getCookiesForTeacher();
        this.mapOfCookiesMethodist = DataForAuthorization.getCookiesForMethodologist();
        this.mapOfCookiesStudent = DataForAuthorization.getCookiesForStudent();
        this.mapOfCookiesPublisher = DataForAuthorization.getCookiesForPublisher();
    }


    public abstract int createNew();

    public abstract void deleteObj(int materialId) throws SQLException;

    public abstract void toModerationStatus(int materialId);

    public abstract void toNewStatus(int materialId);

    public abstract int getLastFromDb();

    public abstract void updModerationStatus(int materialId, int newStatusId) throws SQLException ;
}
