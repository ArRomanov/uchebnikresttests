package ObjFactories;

import DataBase.HelperForQueries;
import DataBase.queries.ForAtomicObjects;
import ResourcesClasses.JsonBodiesForTestTasks;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class TestTask extends ObjectForTest {
    TestTask() {
        super();
    }

    public int createNew() {
        System.out.println("\n...creating of test task...\n");
        int idCreatedTask = given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(JsonBodiesForTestTasks.getBodyForTaskWithSingleAnswer())
                .when().post(STABLE_BASE_URL + EXAM_TASK)
                .then().statusCode(200)
                .extract().path("id");
        System.out.println("\nTest task created: " + idCreatedTask);
        return idCreatedTask;
    }

    public void deleteObj(int materialId) throws SQLException {
        System.out.println("\n...deleting test task...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .header("Accept", "application/json")
                .when().delete(STABLE_BASE_URL + EXAM_TASK + "/" + materialId)
                .then().statusCode(200);
        System.out.println("\n...test task " + materialId + " deleted...\n");
    }

    public void toModerationStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "pending");
        body.put("message", null);
        System.out.println("\n...sending task to moderation...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + materialId + MODERATION)
                .then().statusCode(200);
        body.replace("moderation_status", "accepted");
        given()
                .cookies(mapOfCookiesMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + materialId + MODERATION)
                .then().statusCode(200);
        System.out.println("\n...moderation accepted for task " + materialId + "...\n");
    }

    public void toNewStatus(int materialId) {
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "new");
        body.put("message", null);
        System.out.println("\n...to 'new' moderation status for test task...\n");
        given()
                .cookies(mapOfCookiesTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + materialId + MODERATION)
                .then().statusCode(200);
        System.out.println("\n...test task " + materialId + " is in status 'new'...\n");
    }

    public int getLastFromDb() {
        String idTestTask = ForAtomicObjects.getLastTestTask();
        return Integer.parseInt(idTestTask);
    }

    public void updModerationStatus(int materialId, int newStatusId) throws SQLException {
        String query = "update atomic_objects\n" +
                "set moderation_status=" + newStatusId + "\n" +
                "where id=" + materialId;
        HelperForQueries.executeUpdate(query);
    }
}
