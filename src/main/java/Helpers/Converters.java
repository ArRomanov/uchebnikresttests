package Helpers;

import DataBase.HelperForQueries;
import org.json.JSONObject;

import java.util.List;

public class Converters {

    public static JSONObject parseStringToJson(String context){
        try {
            JSONObject  allContent = new JSONObject(context);
            return allContent;
        } catch (NullPointerException e){
            return new JSONObject();
        }
    }


    public static String parseIdToStringModerationStatus(String idOfModerationStatus) {
        String valueOfModerationStatus = "";
        switch (idOfModerationStatus) {
            case "1":
                valueOfModerationStatus = "new";
                break;
            case "2":
                valueOfModerationStatus = "pending";
                break;
            case "3":
                valueOfModerationStatus = "accepted";
                break;
            case "4":
                valueOfModerationStatus = "rejected";
                break;
        }
        return valueOfModerationStatus;
    }

}
