package Helpers.clearTestData;

import DataBase.HelperForQueries;
import ObjFactories.Factories;
import ObjFactories.ObjectForTest;

import java.sql.SQLException;
import java.util.List;

import static DataForTests.PathsAndIds.ROOT_FOLDER;

public class SpecAtomicAndLesson {

    private static List<String> getMaterialsFromRootFolder(String cmsTypeSpecOrAtomicOrLesson) throws SQLException {
        String cmsType = "";
        switch (cmsTypeSpecOrAtomicOrLesson.toLowerCase()) {
            case "spec":
                cmsType = "TestSpecification";
                break;
            case "lesson":
                cmsType = "LessonTemplate";
                break;
        }
        String queryToDB = "select material_id from folders_materials\n" +
                "where folder_id = " + ROOT_FOLDER + "\n" +
                "and material_type='" + cmsType + "';";
        return HelperForQueries.executeSelectGetListOfRows(queryToDB);
    }

    public static void delAllAtomicAsideFromTaskFromFolder() throws SQLException {
        String query = "select fm.material_id\n" +
                "from folders_materials fm\n" +
                "join atomic_objects ao\n" +
                "on fm.material_id=ao.id\n" +
                "where fm.material_type='AtomicObject'\n" +
                "and ao.logical_type_id not between 26 and 34\n" +
                "and fm.folder_id=" + ROOT_FOLDER;
        List<String> listOfAtomicForDel = HelperForQueries.executeSelectGetListOfRows(query);
        ObjectForTest atomicFactory = Factories.getAtomicFactory();
        String atomicId;
        for (int counter = 0; counter < listOfAtomicForDel.size(); counter++) {
            atomicId = listOfAtomicForDel.get(counter);
            atomicFactory.updModerationStatus(Integer.parseInt(atomicId), 1);
            atomicFactory.deleteObj(Integer.parseInt(atomicId));
        }
    }

    public static void deleteAllTestSpecFromMyFolder() throws SQLException {
        List<String> listOfSpecForDel = getMaterialsFromRootFolder("spec");
        ObjectForTest specFactory = Factories.getSpecFactory();
        String specId;
        for (int counter = 0; counter < listOfSpecForDel.size(); counter++) {
            specId = listOfSpecForDel.get(counter);
            specFactory.updModerationStatus(Integer.parseInt(specId), 1);
            specFactory.deleteObj(Integer.parseInt(specId));
        }
    }

    public static void deleteAllTasksFromMyFolder() throws SQLException {
        String query = "select fm.material_id\n" +
                "from folders_materials fm\n" +
                "join atomic_objects ao\n" +
                "on fm.material_id=ao.id\n" +
                "where fm.material_type='AtomicObject'\n" +
                "and ao.logical_type_id between 26 and 34\n" +
                "and fm.folder_id=" + ROOT_FOLDER;
        List<String> listOfTestTasks = HelperForQueries.executeSelectGetListOfRows(query);
        ObjectForTest taskFactory = Factories.getTaskFactory();
        String taskId;
        for (int counter = 0; counter < listOfTestTasks.size(); counter++) {
            taskId = listOfTestTasks.get(counter);
            taskFactory.updModerationStatus(Integer.parseInt(taskId), 1);
            taskFactory.deleteObj(Integer.parseInt(taskId));
        }
    }

    public static void deleteAllLessonsFromMyFolder() throws SQLException {
        List<String> listOfLessonForDel = getMaterialsFromRootFolder("lesson");
        ObjectForTest lessonFactory = Factories.getLessonFactory();
        String lessonId;
        for (int counter = 0; counter < listOfLessonForDel.size(); counter++) {
            lessonId = listOfLessonForDel.get(counter);
            lessonFactory.updModerationStatus(Integer.parseInt(lessonId), 1);
            lessonFactory.deleteObj(Integer.parseInt(lessonId));
        }
    }
}
