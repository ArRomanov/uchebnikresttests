package Helpers.clearTestData;

import DataBase.HelperForQueries;

import java.sql.SQLException;

public class OtherMaterials {

    public static void deleteAllCreatedAssignments() throws SQLException {
        String query = "delete from assignments where \"name\"='AutotestAssigment' or \"name\"='AutotestAssigmentChanged'";
        HelperForQueries.executeDelete(query);
    }

    public static void deleteAllCreatedAssignmentBookItems() throws SQLException {
        String query = "delete from assignment_book_items where title='AutotestItemChanged' or title='AutotestItem'";
        HelperForQueries.executeDelete(query);
    }

    public static void deleteAllCreatedBooks() throws SQLException {
        String query = "delete from books where title='autoCreateBook' or title='changedTitle'";
        HelperForQueries.executeDelete(query);
    }

    public static void deleteAllCreatedComposedDocs() throws SQLException {
        String query = "delete from composed_documents where name='autoCreatedComposedDoc' or name='autoChangedComposedDoc'";
        HelperForQueries.executeDelete(query);
    }

    public static void deleteDigitalDocsInRootFolder() throws SQLException {
        String query = "delete from digital_documents where name='autoCreatedDigitalDoc' or name='autoChangedDigitalDoc'";
        HelperForQueries.executeDelete(query);
    }
}
