package Helpers;

import JsonClasses.AuthMethod.Profile;
import JsonClasses.AuthMethod.Sessions;
import io.restassured.response.Response;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static DataForTests.PathsAndIds.API_SESSION;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static DataForTests.DataForAuthorization.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;


public class ForAuthorization {
    private static Response respOfSessionMethod;
    private static Profile profileOfRole;

    public static void getAuthorization(String login, String password) {
        Map<String, Object> body = new HashMap<>();
        body.put("login", login);
        body.put("password_hash", encryptPassword(password));
        Response responseSessions = given()
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .when().post(STABLE_BASE_URL + API_SESSION)
                .then().statusCode(200)
                .extract().response();
        respOfSessionMethod = responseSessions;
        setUserIdProfileIdAuthTokenForAllRoles();
    }

    private static void setUserIdProfileIdAuthTokenForAllRoles() {
        int userId;
        int profileId;
        String role;
        List<Profile> listOfProfiles = getProfilesFromResponse();
        for (int numOfProfileInResponse = 0; numOfProfileInResponse < listOfProfiles.size(); numOfProfileInResponse++) {
            profileOfRole = listOfProfiles.get(numOfProfileInResponse);
            role = profileOfRole
                    .getType();
            userId = profileOfRole
                    .getUserId();
            profileId = profileOfRole
                    .getId();
            switch (role) {
                case "teacher":
                    userIdTeacher = userId;
                    profileIdTeacher = profileId;
                    authTokenTeacher = getAuthTokenFromResp();
                    continue;
                case "student":
                    userIdStudent = userId;
                    profileIdStudent = profileId;
                    authTokenStudent = getAuthTokenFromResp();
                    continue;
                case "author":
                    userIdAuthor = userId;
                    profileIdAuthor = profileId;
                    authTokenAuthor = getAuthTokenFromResp();
                    continue;
                case "reader":
                    userIdReader = userId;
                    profileIdReader = profileId;
                    authTokenReader = getAuthTokenFromResp();
                    continue;
                case "methodologist":
                    userIdMethodologist = userId;
                    profileIdMethodologist = profileId;
                    authTokenMethodologist = getAuthTokenFromResp();
                    continue;
                case "publisher":
                    userIdPublisher = userId;
                    profileIdPublisher = profileId;
                    authTokenPublisher = getAuthTokenFromResp();
                    continue;
            }
        }
    }

    private static String getAuthTokenFromResp() {
        return respOfSessionMethod
                .getBody()
                .as(Sessions.class)
                .getAuthenticationToken();
    }

    private static List<Profile> getProfilesFromResponse() {
        return respOfSessionMethod
                .getBody()
                .as(Sessions.class)
                .getProfiles();
    }

    private static String encryptPassword(String password) {
        return DigestUtils.md5Hex(password + "");
    }
}


