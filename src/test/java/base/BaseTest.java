package base;

import DataForTests.DataForAuthorization;
import ObjFactories.Factories;
import ObjFactories.ObjectForTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static Helpers.clearTestData.OtherMaterials.*;
import static Helpers.clearTestData.SpecAtomicAndLesson.*;

public class BaseTest {
    public static Map<String, String> mapOfCookiesForTeacher = new HashMap<>();
    public static Map<String, String> mapOfCookiesForMethodist = new HashMap<>();
    public static Map<String, String> mapOfCookiesForPublisher = new HashMap<>();
    public static Map<String, String> mapOfCookiesForStudent = new HashMap<String, String>();

    public static ObjectForTest atomicFactory;
    public static ObjectForTest lessonFactory;
    public static ObjectForTest specFactory;
    public static ObjectForTest taskFactory;

    @BeforeClass
    public static void getAuthData() {
        mapOfCookiesForTeacher = DataForAuthorization.getCookiesForTeacher();
        mapOfCookiesForPublisher = DataForAuthorization.getCookiesForPublisher();
        mapOfCookiesForMethodist = DataForAuthorization.getCookiesForMethodologist();
        mapOfCookiesForStudent = DataForAuthorization.getCookiesForStudent();

        atomicFactory = Factories.getAtomicFactory();
        lessonFactory = Factories.getLessonFactory();
        specFactory = Factories.getSpecFactory();
        taskFactory = Factories.getTaskFactory();
    }

    @AfterClass()
    public static void deletedCreatedObjects() throws SQLException {
        System.out.println("\n...deleting all materials after tests...\n");
        delAllAtomicAsideFromTaskFromFolder();
        deleteAllCreatedAssignments();
        deleteAllCreatedAssignmentBookItems();
        deleteAllTestSpecFromMyFolder();
        deleteAllLessonsFromMyFolder();
        deleteAllTasksFromMyFolder();
        deleteAllCreatedBooks();
        deleteAllCreatedComposedDocs();
        deleteDigitalDocsInRootFolder();
    }
}
