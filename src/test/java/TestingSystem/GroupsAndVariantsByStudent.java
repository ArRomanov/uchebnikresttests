package TestingSystem;

import DataBase.queries.ForTestGroupsAndVariants;
import DataBase.queries.ForTestSpecs;
import DataForTests.DataForStudentTests;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForStudentTests.CountOfQuestionsInTest;
import static DataForTests.DataForStudentTests.newVariant;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class GroupsAndVariantsByStudent extends BaseTest {

    @Test()
    public static void generateAndPassingNewVariant() {
        int specId = specFactory.createNew();
        specFactory.toModerationStatus(specId);
        Map<String, Object> body = new HashMap<>();
        body.put("spec_id", specId);
        body.put("can_see_result", true);
        body.put("can_reanswer", false);
        body.put("can_switch", false);
        body.put("immediate_result", false);
        body.put("variants_count", 1);
        System.out.println("\n...generation a new group...\n");
        int groupId = given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .header("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + TESTPLAYER_GROUP)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("id");
        String groupFromDB = ForTestGroupsAndVariants.getLastGeneratedTestGroup();
        Assert.assertEquals(groupId, Integer.parseInt(groupFromDB));

        System.out.println("\n...generation a new variant...\n");
        int newVariant = given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .header("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + VARIANTS_GENERATE + "/" + groupId)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().getInt("variants[0].id");
        String variantIdFromDB = ForTestGroupsAndVariants.getLastGeneratedVariant();
        Assert.assertEquals(newVariant, Integer.parseInt(variantIdFromDB));
        DataForStudentTests.newVariant = newVariant;

        System.out.println("\n...getting count of test task in variant...\n");
        int countOfQuestions = given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TEST_PLAYER_VARIANT + "/" + newVariant)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().getInt("tasks_count");
        CountOfQuestionsInTest = countOfQuestions;
        int countOfQuestionsFromDB = ForTestSpecs.getRightAnswerForTestSpec().size();
        Assert.assertEquals(countOfQuestions, countOfQuestionsFromDB);

        System.out.println("\n...starting of test...\n");
        given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body("{}")
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + USER_VARIANT + "/" + newVariant)
                .then().log().body()
                .statusCode(200);
        HashMap<String, String> bodyForStartTest = new HashMap<>();
        bodyForStartTest.put("status", "active");
        bodyForStartTest.put("status_update_type", "update_self");
        given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(bodyForStartTest)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + USER_VARIANT + "/" + newVariant)
                .then().log().body()
                .statusCode(200);
        String testStatus = ForTestGroupsAndVariants.getStatusOfTest();
        Assert.assertEquals(testStatus, "active");

        Map<String, String> numAndRightAnswer = ForTestSpecs.getRightAnswerForTestSpec();
        System.out.println("\n...sending of test answers by student for checking...\n");
        for (Map.Entry entry : numAndRightAnswer.entrySet()) {
            given()
                    .cookies(mapOfCookiesForStudent)
                    .contentType(JSON)
                    .headers("Accept", "application/json")
                    .body(entry.getValue())
                    .log().method().log().uri().log().cookies().log().body()
                    .when().post(STABLE_BASE_URL + ANSWER + "/" + newVariant + "/" + entry.getKey())
                    .then().log().body()
                    .statusCode(200);
        }
        int countOfAnswerFromDB = ForTestGroupsAndVariants.getCountOfStudentAnswers();
        Assert.assertEquals(numAndRightAnswer.size(), countOfAnswerFromDB);

        Map<String, String> bodyForFinishTest = new HashMap<>();
        bodyForFinishTest.put("status", "stop");
        bodyForFinishTest.put("status_update_type", "update_self");
        System.out.println("\n...finish of test...\n");
        given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(bodyForFinishTest)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + USER_VARIANT + "/" + newVariant)
                .then().log().body()
                .statusCode(200);
        testStatus = ForTestGroupsAndVariants.getStatusOfTest();
        Assert.assertEquals(testStatus, "stop");

        System.out.println("\n...getting variant by self...\n");
        given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_ANSWERS_VARIANT + "/" + newVariant + "/user/self")
                .then().log().body()
                .statusCode(200);
    }

    @DataProvider
    public Object[][] getNumAndTypeOfTasksInVariant() {
        return DataForStudentTests.getNumAndTypeOfTasksInVariant();
    }

    @Test(dataProvider = "getNumAndTypeOfTasksInVariant", dependsOnMethods = "generateAndPassingNewVariant")
    public static void getVariantsOfAnswersForTestTasks(String numOfTask, String typeOfString) {
        System.out.println("\n...getting of variants answers for test tasks...\n");
        given()
                .cookies(mapOfCookiesForStudent)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + VARIANTS_OF_TEST_TASK + "/" + newVariant + "/" + numOfTask)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("answer.type", equalTo(typeOfString));
    }
}
