package TestingSystem;

import DataBase.queries.ForCms;
import DataBase.queries.Other;
import Helpers.ListenerForLogs;
import base.BaseTest;
import io.restassured.response.Response;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

import static DataForTests.DataForAuthorization.userIdTeacher;
import static DataForTests.DataForTeacherTests.getLevelsForSubjects;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;

@Listeners(ListenerForLogs.class)
public class CommonGETMethods extends BaseTest {

    @Test
    public static void getLogicalTypes() {
        System.out.println("\n...getting logical types...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().uri().log().cookies()
                .when()
                .get(STABLE_BASE_URL + LOGICAL_TYPES)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void denyAccessForMethodologistToTeacherActivity() {
        System.out.println("\n...checking deny access for methodologist to teacher activity...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().uri().log().cookies()
                .when()
                .get(STABLE_BASE_URL + TDESK_ACADEMIC_YEAR)
                .then().log().body()
                .statusCode(403);
    }

    @Test
    public void getUserInfoById() {
        System.out.println("\n...getting user's info by id...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_USERS + "/" + userIdTeacher)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("id", equalTo(userIdTeacher))
                .body("deleted_at", equalTo(null));
    }

    @Test
    public void getTeacherAuthInfo() {
        System.out.println("\n...getting teacher auth data...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_USER_INFO)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getQuotaInfo() {
        System.out.println("\n...getting quota info...\n");
        Response response = (Response) given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_QUOTA)
                .then().log().body()
                .statusCode(200)
                .extract();
        String qLimitFromMeth = response.jsonPath().getString("quota_limit");
        String qCurrentFromMeth = response.jsonPath().getString("quota_current");
        List<String> quotaInfoListFromBD = Other.getQuotaInfo(mapOfCookiesForTeacher.get("user_id"));
        assertEquals(qLimitFromMeth, quotaInfoListFromBD.get(0));
        assertEquals(qCurrentFromMeth, quotaInfoListFromBD.get(1));
    }

    @Test
    public void getCountOfUnDeletedMaterials() {
        System.out.println("\n...getting count of deleted materials...\n");
        int countOfAtomicFromDB = ForCms.getCountOfAtomicObj();
        int countOfAtomicFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_COUNT_OF_MATERIALS)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("atomic_objects_count");
        assertEquals(countOfAtomicFromDB, countOfAtomicFromResp);
    }

    @Test
    public void getEducationLevels() {
        System.out.println("\n...getting education levels...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_EDUCATION_LEVELS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getStudyingLevels() {
        System.out.println("\n...getting studying levels...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_STUDYING_LEVELS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getAnswerTypes() {
        System.out.println("\n...getting answer's types...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_ANSWER_TYPES)
                .then().log().body()
                .statusCode(200);
    }

    @DataProvider
    public Object[][] getLevelsOfEducation() {
        return getLevelsForSubjects();
    }

    @Test(dataProvider = "getLevelsOfEducation")
    public void getAllSubjects(String levelOfEducationNum) {
        System.out.println("\n...getting all subjects...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_SUBJECTS + "/" + levelOfEducationNum)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getSubjectsWithControllableItems() {
        System.out.println("\n...getting subjects with controllable items...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_SUBJECTS + "?" + "with_controllable_items=true")
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getAllEquipments() {
        System.out.println("\n...getting all equipments...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when()
                .get(STABLE_BASE_URL + API_EQUIPMENT)
                .then().log().body()
                .statusCode(200);

    }

    @Test
    public void getGenres() {
        System.out.println("\n...getting genres...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_GENRES)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getLicenseTypes() {
        System.out.println("\n...getting types of license...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_LICENSE_TYPE)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getBookTypes() {
        System.out.println("\n...getting types of book...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + BOOK_TYPES)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getPublisherActivationCode() {
        System.out.println("\n...getting publisher activation code...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + PUBLISHER_ACTIVATION_CODE)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getMaterialCountBySubjects() {
        System.out.println("\n...getting count of material by subjects...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIAL_COUNT_BY_SUBJECTS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getMyBaseFolders() {
        System.out.println("\n...getting my base folder...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_FOLDERS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getAcademicYear() {
        System.out.println("\n...getting academic year...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().all()
                .when().get(STABLE_BASE_URL + TDESK_ACADEMIC_YEAR)
                .then().log().all()
                .statusCode(200);
    }
}
