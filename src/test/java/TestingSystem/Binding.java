package TestingSystem;

import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static DataBase.queries.ForAtomicObjects.getLastTestTask;
import static DataBase.queries.ForAtomicObjects.getModerationStatusOfTestTask;
import static DataBase.queries.ForTestSpecs.getLastTestSpec;
import static DataBase.queries.ForTestSpecs.getModerationStatusOfSpec;
import static DataForTests.DataForMethodologistTests.ACCEPTED_MODERATION_STATUS;
import static DataForTests.DataForTeacherTests.*;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;


@Listeners(ListenerForLogs.class)
public class Binding extends BaseTest {

    private static Map<String, Object> getBodyForCreateBindingWithNewElements(final String SPEC_OR_TASK) {
        Map<String, Object> body = new HashMap<>();
        body.put("material_id", newLessonTemplate);
        body.put("material_type", "lesson");
        switch (SPEC_OR_TASK.toLowerCase()) {
            case "spec":
                body.put("test_material_id", specFactory.createNew());
                body.put("test_material_type", "spec");
                break;
            case "task":
                body.put("test_material_id", taskFactory.createNew());
                body.put("test_material_type", "task");
                break;
        }
        return body;
    }

    private static void deleteBinding(int bindingId) {
        createAndDeleteBindingLessonAndSpec();
        System.out.println("\n...deleting binding of lesson...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "*/*")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + BINDING + "/" + bindingId)
                .then().statusCode(200);
    }

    @Test
    public static void createAndDeleteBindingLessonAndSpec() {
        newLessonTemplate = lessonFactory.createNew();
        Map<String, Object> body = getBodyForCreateBindingWithNewElements("spec");
        System.out.println("\n...creating binding of lesson and test spec...\n");
        newBindingId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "*/*")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + BINDING)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("binded_entity_info.id", equalTo(newTestSpec))
                .extract().jsonPath().get("binding_id");
        int specIdInt = specFactory.getLastFromDb();
        System.out.println("\n...getting info of specification by binding...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_SPECIFICATION + "/bind/" + newBindingId)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("spec_id", equalTo(specIdInt));
        System.out.println("\n...getting info about binding...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .get(STABLE_BASE_URL + BINDING + "/" + newBindingId)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("id", equalTo(body.get("test_material_id")));
        deleteBinding(newTestSpec);
    }

    @Test
    public static void createBindingLessonAndTask() {
        newLessonTemplate = lessonFactory.createNew();
        Map<String, Object> body = getBodyForCreateBindingWithNewElements("task");
        System.out.println("\n...creating binding of lesson and test task...\n");
        int idTestTask = Integer.parseInt(getLastTestTask());
        newBindingId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "*/*")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + BINDING)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("binded_entity_info.id", equalTo(idTestTask))
                .extract().jsonPath().get("binding_id");
        System.out.println("\n...getting training test's info by binding id...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .get(STABLE_BASE_URL + TRAINING_TEST + "/" + newBindingId)
                .then().log().body()
                .statusCode(200);
        deleteBinding(newTestSpec);
    }

    @Test
    public static void createBindingLessonAndSpecAndTask() {
        newLessonTemplate = lessonFactory.createNew();
        Map<String, Object> body_for_spec_binding = getBodyForCreateBindingWithNewElements("spec");
        Map<String, Object> body_for_task_binding = getBodyForCreateBindingWithNewElements("task");
        System.out.println("\n...creating binding of lesson and test spec...\n");
        newBindingId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "*/*")
                .body(body_for_spec_binding)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + BINDING)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("binding_id");
        System.out.println("\n...creating binding of lesson and test task...\n");
        newBindingId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "*/*")
                .body(body_for_task_binding)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + BINDING)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("binding_id");
    }

    @Test
    public void moderationTaskAndSpecBoundToLesson() {
        createBindingLessonAndSpecAndTask();
        String taskId = getLastTestTask();
        String specId = getLastTestSpec();
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "accepted");
        body.put("message", null);
        System.out.println("\n...sending to moderation lesson with bound materials...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + BINDING_MODERATION_LESSON + "/" + newLessonTemplate)
                .then().statusCode(200);
        String statusFromBDForTask = getModerationStatusOfTestTask(Integer.parseInt(taskId));
        String statusFromBDForSpec = getModerationStatusOfSpec(Integer.parseInt(specId));
        assertEquals(statusFromBDForSpec, ACCEPTED_MODERATION_STATUS);
        assertEquals(statusFromBDForTask, ACCEPTED_MODERATION_STATUS);
    }
}
