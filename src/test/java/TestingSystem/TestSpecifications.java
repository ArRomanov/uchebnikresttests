package TestingSystem;

import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static DataBase.queries.ForTestSpecs.getLastTestSpec;
import static DataBase.queries.ForTestSpecs.getModerationStatusOfSpec;
import static DataForTests.DataForMethodologistTests.*;
import static DataForTests.PathsAndIds.*;
import static Helpers.Converters.parseIdToStringModerationStatus;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class TestSpecifications extends BaseTest {

    @Test
    public static void getNewSpec() {
        int specFromDb = specFactory.getLastFromDb();
        System.out.println("\n...getting new test specification...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_SPEC + "/" + specFromDb)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("id", equalTo(specFromDb));
    }

    @Test
    public static void getModerationStatus() {
        int specId = specFactory.getLastFromDb();
        System.out.println("\n...getting moderation status of test specification...\n");
        String valueOfModerationStatusFromMethod = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_SPEC + "/" + specId + MODERATION)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("moderation_status");
        String idOfStatusFromBD = getModerationStatusOfSpec(specId);
        String valueOfModerationStatusFromBD = parseIdToStringModerationStatus(idOfStatusFromBD);
        assertEquals(valueOfModerationStatusFromBD, valueOfModerationStatusFromMethod);
    }

    @Test
    public static void specToPendingToNewAndDelete() {
        int newTestSpec = specFactory.createNew();
        int testSpecFromDb = specFactory.getLastFromDb();
        assertEquals(newTestSpec, testSpecFromDb);

        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "pending");
        body.put("message", null);
        System.out.println("\n...sending of test specifications to moderation...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + newTestSpec + MODERATION)
                .then().statusCode(200);
        String statusOfModerationFromDB = getModerationStatusOfSpec(newTestSpec);
        assertEquals(statusOfModerationFromDB, FOR_APPROVAL_MODERATION_STATUS);

        body.replace("moderation_status", "new");
        System.out.println("\n...cancel sending of test specifications to moderation...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + newTestSpec + MODERATION)
                .then().statusCode(200);
        statusOfModerationFromDB = getModerationStatusOfSpec(newTestSpec);
        assertEquals(statusOfModerationFromDB, STATUS_NEW);

        System.out.println("\n...deleting of test specifications...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + EXAM_SPEC + "/" + newTestSpec)
                .then().statusCode(200);
        String nextTestSpecFromDB = getLastTestSpec();
        assertNotEquals(newTestSpec, nextTestSpecFromDB);
    }

    @Test
    public static void getVersionInfo() {
        String specId = getLastTestSpec();
        System.out.println("\n...getting info about test specification...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + API_SPECIFICATION + "/" + specId)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void moderationSpecPass() {
        int specId = specFactory.createNew();
        specFactory.toModerationStatus(specId);
        String statusFromBD = getModerationStatusOfSpec(specId);
        assertEquals(statusFromBD, ACCEPTED_MODERATION_STATUS);

        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "rejected");
        body.put("message", "it's done by autoTest");
        System.out.println("\n...rejected test specification by moderator...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_SPEC + "/" + specId + MODERATION)
                .then().statusCode(200);
        statusFromBD = getModerationStatusOfSpec(specId);
        assertEquals(statusFromBD, REJECTED_MODERATION_STATUS);

        System.out.println("\n...getting moderation history of test specification...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_SPEC + "/" + specId + MODERATION_HISTORY)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("[0].moderation_status", equalTo("rejected"))
                .body("[1].moderation_status", equalTo("accepted"))
                .body("[2].moderation_status", equalTo("pending"))
                .body("[3].moderation_status", equalTo("new"));
    }
}
