package TestingSystem;

import DataBase.queries.ForAtomicObjects;
import DataBase.queries.ForTestGroupsAndVariants;
import DataForTests.DataForTeacherTests;
import base.BaseTest;
import io.restassured.path.json.JsonPath;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static DataForTests.DataForAuthorization.userIdStudent;
import static DataForTests.DataForTeacherTests.groupOfGeneratedVariants;
import static DataForTests.DataForTeacherTests.newVariant;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

//@Listeners(ListenerForLogs.class)
public class GroupsAndVariantsByTeacher extends BaseTest {


    @DataProvider
    public static Object[][] getIdsTaskOfVariant() {
        return DataForTeacherTests.getIdsTaskOfVariant();
    }

    @Test(dataProvider = "getIdsTaskOfVariant")
    public static void getAnswersOfStudent(String taskId) {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        String userId = ForTestGroupsAndVariants.getUserIdByVariant(variantId);
        System.out.println("\n...getting answers of student...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + ANSWER + "/variant/" + variantId + "/task/" + taskId + "/user/" + userId)
                .then().log().body()
                .statusCode(200);
    }


    @Test(dataProvider = "getIdsTaskOfVariant")
    public static void getRightAnswer(String taskId) {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        System.out.println("\n...getting task's right answers of variants...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TASK_OF_VARIANT + "/" + taskId + "/variant/" + variantId)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void generateAndDistributionVariants() {
        int specId = specFactory.createNew();
        specFactory.toModerationStatus(specId);
        Map<String, Object> bodyforGenerateVariant = new HashMap<>();
        bodyforGenerateVariant.put("spec_id", specId);
        bodyforGenerateVariant.put("group_id", 66);
        bodyforGenerateVariant.put("can_see_result", true);
        bodyforGenerateVariant.put("can_reanswer", true);
        bodyforGenerateVariant.put("can_switch", true);
        bodyforGenerateVariant.put("immediate_result", true);
        bodyforGenerateVariant.put("variants_count", 1);
        System.out.println("\n...generating variant's group...\n");
        int groupOfVariantsId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(bodyforGenerateVariant)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + VARIANTS_GENERATE)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("generated_variants_id");
        String generatedGroupFromBD = ForTestGroupsAndVariants.getLastGeneratedTestGroup();
        assertEquals(groupOfVariantsId, Integer.parseInt(generatedGroupFromBD));
        groupOfGeneratedVariants = groupOfVariantsId;

        System.out.println("\n...getting all variants by test group...\n");
        List<Integer> variants = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_VARIANT_GROUP + "/" + groupOfVariantsId)
                .then().log().body()
                .statusCode(200)
                .extract()
                .jsonPath().get("variant_id");
        String variantIdFromDB = ForTestGroupsAndVariants.getLastGeneratedVariant();
        int variantId = variants.get(0);
        Assert.assertEquals(Integer.parseInt(variantIdFromDB), variantId);

        System.out.println("\n...getting group id by variant id...\n");
        int groupId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + GROUP_VARIANT + "/" + variantId)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("[0].generated_variants_id");
        Assert.assertEquals(groupId, groupOfVariantsId);

        JSONArray studentsArray = new JSONArray();
        studentsArray.put(userIdStudent);
        JSONObject variantObj = new JSONObject();
        variantObj.put("variant_id", variantId);
        variantObj.put("users", studentsArray);
        JSONArray variantsArray = new JSONArray();
        variantsArray.put(variantObj);
        JSONObject body = new JSONObject();
        body.put("variants", variantsArray);
        System.out.println("\n...distribution students on variants...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body.toString().replace("=", ":"))
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + VARIANTS + "/" + groupOfVariantsId)
                .then().log().body()
                .statusCode(200);
        boolean success = ForTestGroupsAndVariants.isUsersBindWithVariants(variantId, userIdStudent);
        assertTrue(success);

        String testStatusByTeacher = "stop";
        Map<String, Object> bodyForStop = new HashMap<>();
        bodyForStop.put("status", testStatusByTeacher);
        System.out.println("\n...stopping test by teacher...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .body(bodyForStop)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + VARIANTS + "/" + groupOfGeneratedVariants)
                .then().statusCode(200);
        String testStatusFromDB = ForTestGroupsAndVariants.getStatusOfTestVariants(Integer.toString(variantId));
        Assert.assertEquals(testStatusFromDB, testStatusByTeacher);
    }

    @Test(priority = 1)
    public static void getGeneratedVariants() {
        System.out.println("\n...getting generated variant...\n");
        int variant = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + VARIANTS_GENERATE + "/" + groupOfGeneratedVariants)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("variants[0].id");
        newVariant = variant;
        String variantIdFromBD = ForTestGroupsAndVariants.getVariantsByGroupId(groupOfGeneratedVariants).get(0);
        Assert.assertEquals(variant, Integer.parseInt(variantIdFromBD));
    }

    @Test(priority = 1)
    public static void getAnswerOfGeneratedVariant() {
        System.out.println("\n...getting answers of generated variant by teacher...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + VARIANTS + "/" + groupOfGeneratedVariants + "/answers")
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getAnswerByVariantAndTaskAndUser() {
        String variantIdByStudent = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        String taskId = ForTestGroupsAndVariants.getTaskIdOfVariant(variantIdByStudent).get(0);
        System.out.println("\n...getting answer by variant, task and user...\n");
        JsonPath response = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_ANSWERS_VARIANT + "/" + variantIdByStudent + "/task/" + taskId + "/user/" + userIdStudent)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath();
        Assert.assertEquals(response.getString("user_id"), "[" + userIdStudent + "]");
        Assert.assertEquals(response.getString("variant_id"), "[" + variantIdByStudent + "]");
        Assert.assertEquals(response.getString("task_id"), "[" + taskId + "]");
    }

    @Test
    public static void getInfoUserByVariant() {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        System.out.println("\n...getting user's info by variant...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_USER_VARIANT + "/" + variantId)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getTaskIdsOfVariants() {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        System.out.println("\n...getting task's id of variants...\n");
        List<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_TASK_VARIANT + "/" + variantId)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().getList("id");
        List<String> idsFromBD = ForTestGroupsAndVariants.getTaskIdOfVariant(variantId);
        Collections.sort(ids);
        Collections.sort(idsFromBD);
        Assert.assertEquals(ids.toString(), idsFromBD.toString());
    }


    @Test
    public static void getAnswerStatusOfStudent() {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        String studentId = ForTestGroupsAndVariants.getUserIdByVariant(variantId);
        System.out.println("\n...getting answer's status of student...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_VARIANT + "/" + variantId + "/answer/1/user/" + studentId)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getVariantByAnyStudent() {
        String variantId = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        String userId = ForTestGroupsAndVariants.getUserIdByVariant(variantId);
        System.out.println("\n...getting variant by any student...\n");
        given()
                .contentType(JSON)
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_ANSWERS_VARIANT + "/" + variantId + "/user/" + userId)
                .then().log().body()
                .statusCode(200);
    }


    @Test
    public static void putRatingForOpenAnswer() throws SQLException {
        List<String> studentAndVariantAndTask = ForAtomicObjects.getTaskWithOpenAnswer();
        String studentId = studentAndVariantAndTask.get(0);
        String variantId = studentAndVariantAndTask.get(1);
        String taskId = studentAndVariantAndTask.get(2);
        boolean ratingByTeacher = true;
        Map<String, Object> body = new HashMap<>();
        body.put("user_id", Integer.parseInt(studentId));
        body.put("is_right", ratingByTeacher);
        System.out.println("\n...putting rating for open answer...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + ANSWER + "/" + variantId + "/task/" + taskId)
                .then().statusCode(200);
        try {
            boolean ratingFromDB = ForTestGroupsAndVariants.getRaitingOfTask(variantId, taskId);
            Assert.assertEquals(ratingByTeacher, ratingFromDB);
        } catch (Exception e) {
            System.out.println("Problem with getting boolean from BD or not found needed answer for test");
            assertTrue(false);
        }
    }

    @Test
    public static void getAnswersByVariant() {
        String variantIdFromBD = ForTestGroupsAndVariants.getLastFinishedVariantForSpecByCurrentTeacher();
        String taskId = ForTestGroupsAndVariants.getTaskIdOfVariant(variantIdFromBD).get(0);
        System.out.println("\n...getting answer by variants and task...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_ANSWERS_VARIANT + "/" + variantIdFromBD + "/task/" + taskId)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("[0].variant_id", equalTo(variantIdFromBD))
                .body("[0].task_id", equalTo(taskId));

        System.out.println("\n...getting answer of variant...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TDESK_ANSWERS_VARIANT + "/" + variantIdFromBD)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("[0].variant_id", equalTo(variantIdFromBD));
    }

}
