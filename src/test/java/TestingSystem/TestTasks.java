package TestingSystem;

import DataBase.queries.ForAtomicObjects;
import DataForTests.DataForTeacherTests;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static DataBase.queries.ForAtomicObjects.getLastAtomicObject;
import static DataBase.queries.ForAtomicObjects.getModerationStatusOfTestTask;
import static DataForTests.DataForMethodologistTests.*;
import static DataForTests.DataForTeacherTests.addTestTask;
import static DataForTests.PathsAndIds.*;
import static Helpers.Converters.parseIdToStringModerationStatus;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class TestTasks extends BaseTest {

    @DataProvider
    public Object[][] getJsonBodiesForCreateTestTask() throws IOException, InvocationTargetException, IllegalAccessException {
        return DataForTeacherTests.getJsonBodiesForCreateTestTask();
    }

    @Test(dataProvider = "getJsonBodiesForCreateTestTask")
    public static void createDifferentTestTask(String body) {
        System.out.println("\n...creating of test task...\n");
        int idCreatedTask = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + EXAM_TASK)
                .then().log().body()
                .statusCode(200)
                .extract().path("id");
        assertEquals(Integer.toString(idCreatedTask), getLastAtomicObject());
        addTestTask(idCreatedTask);
    }

    @Test
    public static void deleteTestTask() {
        int newTestTask = taskFactory.createNew();
        System.out.println("\n...deleting of test task...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + EXAM_TASK + "/" + newTestTask)
                .then().log().body()
                .statusCode(200);
        int oldTestTask = taskFactory.getLastFromDb();
        assertNotEquals(newTestTask, oldTestTask);
    }

    @Test
    public static void putAndCancelModeration() {
        int newTestTask = taskFactory.createNew();
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "pending");
        body.put("message", null);
        System.out.println("\n...sending task to moderation...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + newTestTask + MODERATION)
                .then().statusCode(200);
        String statusOfModerationFromDB = getModerationStatusOfTestTask(newTestTask);
        assertEquals(statusOfModerationFromDB, FOR_APPROVAL_MODERATION_STATUS);

        System.out.println("\n...getting moderation status...\n");
        String moderationStatusFromMeth = given()
                .cookies(mapOfCookiesForTeacher)
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_TASK + "/" + newTestTask + MODERATION)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().getString("moderation_status");
        String idModerationStatusFromBD = getModerationStatusOfTestTask(newTestTask);
        String stringModerationStatusFromBD = parseIdToStringModerationStatus(idModerationStatusFromBD);
        assertEquals(moderationStatusFromMeth, stringModerationStatusFromBD);

        body.replace("moderation_status", "new");
        System.out.println("\n...cancel task's moderation...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + newTestTask + MODERATION)
                .then().statusCode(200);
        statusOfModerationFromDB = getModerationStatusOfTestTask(newTestTask);
        assertEquals(statusOfModerationFromDB, STATUS_NEW);
    }

    @Test
    public static void getTestTaskForModeration() {
        System.out.println("\n...getting test task for moderation...\n");
        int testTaskId = given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL + "?moderation_status=2&type=atomic_objects&content_type=test")
                .then().statusCode(200)
                .log().body()
                .extract().jsonPath().get("material_id[1]");

        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "accepted");
        body.put("message", null);
        System.out.println("\n...accepting test task by moderator...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + testTaskId + MODERATION)
                .then().statusCode(200);
        String statusFromBD = getModerationStatusOfTestTask(testTaskId);
        assertEquals(statusFromBD, ACCEPTED_MODERATION_STATUS);

        body.replace("moderation_status", "rejected");
        body.replace("message", "it's done by autoTest");
        System.out.println("\n...rejecting test task by moderator...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().put(STABLE_BASE_URL + EXAM_TASK + "/" + testTaskId + MODERATION)
                .then().statusCode(200);
        statusFromBD = getModerationStatusOfTestTask(testTaskId);
        assertEquals(statusFromBD, REJECTED_MODERATION_STATUS);
    }

    @Test
    public void getTaskModerationHistory() {
        putAndCancelModeration();
        String taskId = ForAtomicObjects.getLastTestTask();
        System.out.println("\n...getting moderation history of test task...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + EXAM_TASK + "/" + taskId + MODERATION_HISTORY)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("[0].moderation_status", equalTo("pending"))
                .body("[1].moderation_status", equalTo("new"));
    }

}
