package TestingSystem;

import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForTeacherTests.newFolderId;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class CommonPOSTandDELETEMethods extends BaseTest {

    @Test
    public static void createFolder() {
        String nameForNewFolder = new SimpleDateFormat("yyyy-MM-dd - HH:mm:ss:SS").format(new Date());
        Map<String, Object> body = new HashMap<>();
        body.put("folder_type", "personal");
        body.put("parent_folder_id", ROOT_FOLDER);
        body.put("name", nameForNewFolder);
        body.put("published", "true");
        System.out.println("\n...creating folder...\n");
        newFolderId = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().cookies().log().body()
                .when().post(STABLE_BASE_URL + ALL_FOLDERS_URL)
                .then().log().body()
                .statusCode(201)
                .assertThat()
                .body("name", equalTo(nameForNewFolder))
                .extract().jsonPath().getInt("id");
    }

    @Test
    public void getActivityHistoryMaterials() {
        String currentData = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        long dateYesterday = new Date().getTime() - 86400000L;
        Date yesterdayDate = new Date(dateYesterday);
        String yesterdayDateStr = new SimpleDateFormat("yyyy-MM-dd").format(yesterdayDate);
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> date = new HashMap<>();
        date.put("start_date", yesterdayDateStr);
        date.put("end_date", currentData);
        body.put("date_range", date);
        body.put("filters", new HashMap<>());
        System.out.println("\n...getting activity history of materials...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().cookies().log().body()
                .when().post(STABLE_BASE_URL + TDESK_ACTIVITY_HISTORY_MATERIAL)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getActivityMaterials() {
        long _24Hours = 86400000L;
        String currentData = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        long dateYesterday = new Date().getTime() - _24Hours;
        Date yesterdayDate = new Date(dateYesterday);
        String yesterdayDateStr = new SimpleDateFormat("yyyy-MM-dd").format(yesterdayDate);
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> date = new HashMap<>();
        date.put("start_date", yesterdayDateStr);
        date.put("end_date", currentData);
        body.put("date_range", date);
        body.put("filters", new HashMap<>());
        System.out.println("\n...getting activity of materials...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + TDESK_ACTIVITY_MATERIAL)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void deleteFolder() {
        CommonPOSTandDELETEMethods.createFolder();
        System.out.println("\n...deleting folder...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + ALL_FOLDERS_URL + "/" + newFolderId)
                .then().statusCode(204);
    }

    @Test
    public void getActivityClasses() {
        Map<String, Object> body = new HashMap<>();
        body.put("academic_year_id", 4);
        System.out.println("\n...getting activity of classes...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + TDESK_ACTIVITY_CLASSES)
                .then().log().body()
                .statusCode(200);
    }
}
