package CmsTests.v2;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import Helpers.clearTestData.SpecAtomicAndLesson;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static DataForTests.DataForAuthorization.userIdTeacher;
import static DataForTests.DataForCms.getCurrentDateYearMmDd;
import static DataForTests.DataForCms.getCurrentTimeHhMmSs;
import static DataForTests.DataForMethodologistTests.ACCEPTED_MODERATION_STATUS;
import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.testng.Assert.assertEquals;

@Listeners(ListenerForLogs.class)
public class Materials extends BaseTest {

    @Test
    public void getAllMaterials() {
        System.out.println("\n...getting of all materials...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v2+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void getMaterialWithSearchAndTypeParams() {
        int specId = specFactory.getLastFromDb();
        System.out.println("\n...getting of materials by parameter of types...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL + "?type=test_specifications&search=" + specId)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, specId);
        } else {
            Assert.fail("count of specifications more then one");
        }
    }

    @Test
    public void getMaterialWithFolderParams() throws SQLException {
        SpecAtomicAndLesson.deleteAllTestSpecFromMyFolder();
        SpecAtomicAndLesson.delAllAtomicAsideFromTaskFromFolder();
        SpecAtomicAndLesson.deleteAllLessonsFromMyFolder();
        int specId = specFactory.createNew();
        System.out.println("\n...getting of materials by parameter of folder...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL + "?folder_id=" + ROOT_FOLDER)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, specId);
        } else {
            Assert.fail("count of elements more then one");
        }
    }

    @Test
    public void getMaterialWithModerStatusParams() throws SQLException {
        SpecAtomicAndLesson.deleteAllTestSpecFromMyFolder();
        int specId = specFactory.createNew();
        specFactory.toModerationStatus(specId);
        System.out.println("\n...getting of materials by parameter of status...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("type", "test_specifications")
                .param("search", "autoSpec")
                .param("user_id", userIdTeacher)
                .param("moderation_status", ACCEPTED_MODERATION_STATUS)
                .param("created_at[from]", getCurrentDateYearMmDd())
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, specId);
        } else {
            Assert.fail("count of specifications more then one");
        }
    }

    @Test
    public void getMaterialWithSubjectIdsParams() throws SQLException {
        SpecAtomicAndLesson.deleteAllTestSpecFromMyFolder();
        int specId = specFactory.createNew();
        System.out.println("\n...getting of materials by parameter of subject...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("type", "test_specifications")
                .param("search", "autoSpec")
                .param("subject_ids", SUBJECT_ID_HARD_CODE)     //I'm sorry for this hard-code, it's universal id for all created specs by autotests
                .param("created_at[from]", getCurrentDateYearMmDd())
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, specId);
        } else {
            Assert.fail("count of specifications more then one");
        }
    }

    @Test
    public void getMaterialWithEducationLevelIdsParams() throws SQLException {
        SpecAtomicAndLesson.deleteAllTestSpecFromMyFolder();
        int specId = specFactory.createNew();
        System.out.println("\n...getting of materials by parameter of education level...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("type", "test_specifications")
                .param("search", "autoSpec")
                .param("education_level_ids", EDUCATION_LEVEL_ID_HARD_CODE)    //I'm sorry for this hard-code, it's universal id for all created specs by autotests
                .param("created_at[from]", getCurrentDateYearMmDd())
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, specId);
        } else {
            Assert.fail("count of specifications more then one");
        }
    }

    @Test
    public void getMaterialWithControllableItemIdParams() throws SQLException {
        SpecAtomicAndLesson.delAllAtomicAsideFromTaskFromFolder();
        int testTaskId = taskFactory.createNew();
        System.out.println("\n...getting of materials by parameter of controllable item id...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("type", "atomic_objects")
                .param("controllable_item_ids", CONTROLLABLE_ITEM_ID_FOR_CMS_ONLY)    //I'm sorry for this hard-code, it's value of controllable from body testTaskForCms.json
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        int specIdFromResp = ids.get(0);
        if (ids.size() == 1) {
            assertEquals(specIdFromResp, testTaskId);
        } else {
            Assert.fail("count of atomic objects more then one");
        }
    }

    @Test
    public void getMaterialWithStudyingLevelIdParams() throws SQLException {
        System.out.println("\n...getting of materials by parameter of studying level...\n");
        ArrayList<String> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("studying_level_ids", FAKE_STUDYING_LEVEL_ID)
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        assertEquals(ids.size(), 0);
    }

    @Test(enabled = false)
    //For now I don't understand how works api/materials searching on date's parameters
    public void getMaterialWithDateToParams() throws SQLException {
        int specIdForTest = specFactory.createNew();
        ForCms.updSpecDateCreationForTest("2014-09-08 14:36:58", specIdForTest);
        String specIdFromGET = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("created_at[to]", "2014-12-08")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().getString("material_id");
        String realDate = getCurrentDateYearMmDd() + " " + getCurrentTimeHhMmSs();
        ForCms.updSpecDateCreationForTest(realDate, specIdForTest);
        assertEquals(specIdForTest, Integer.parseInt(specIdFromGET));
    }

    @Test
    public void getMaterialWithPerPageParam() throws SQLException {
        final String ELEMENTS_COUNT_ON_PAGE = "13";                   //random value until 20
        System.out.println("\n...getting of materials by parameter of per page...\n");
        ArrayList<Integer> ids = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("per_page", ELEMENTS_COUNT_ON_PAGE)
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("material_id");
        String countOfElementsInResp = Integer.toString(ids.size());
        assertEquals(countOfElementsInResp, ELEMENTS_COUNT_ON_PAGE);
    }

    @Test
    public void getMaterialWithPageParam() throws SQLException {
        final String CURRENT_PAGE = "10";                            //random value until 10
        System.out.println("\n...getting of materials by parameter of param page...\n");
        String currentPageFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "*/*")
                .param("page", CURRENT_PAGE)
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + MATERIALS_URL)
                .then().log().body()
                .statusCode(200)
                .extract().response().getHeader("X-Pagination-Current-Page");
        assertEquals(CURRENT_PAGE, currentPageFromResp);
    }
}
