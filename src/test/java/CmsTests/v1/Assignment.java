package CmsTests.v1;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static DataForTests.DataForCms.newAssignment;
import static DataForTests.PathsAndIds.CMS_ASSIGNMENT;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static ResourcesClasses.JsonBodiesForCms.getBodyForChangeAssignment;
import static ResourcesClasses.JsonBodiesForCms.getBodyForNewAssignment;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class Assignment extends BaseTest {

    @Test
    public static void getAllAssignments() {
        System.out.println("\n...getting all assignments...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_ASSIGNMENT)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getAssignmentById() {
        String assignmentIdFromDB = ForCms.getLastAssignment();
        System.out.println("\n...getting assignment by id...\n");
        int assignmentIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_ASSIGNMENT + "/" + assignmentIdFromDB)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("id");
        assertEquals(Integer.parseInt(assignmentIdFromDB), assignmentIdFromResp);
    }

    @Test
    public static void createNewAssignment() {
        System.out.println("\n...creating new assignment...\n");
        int assignmentIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForNewAssignment())
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_ASSIGNMENT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        newAssignment = assignmentIdFromResp;
        String assignmentIdFromDB = ForCms.getLastAssignment();
        assertEquals(Integer.parseInt(assignmentIdFromDB), assignmentIdFromResp);
    }

    @Test
    public static void deleteAssignment() {
        Assignment.createNewAssignment();
        System.out.println("\n...deleting of assignment...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies().log().body()
                .when().delete(STABLE_BASE_URL + CMS_ASSIGNMENT + "/" + newAssignment)
                .then().statusCode(204);
        String assignmentIdFromDB = ForCms.getLastAssignment();
        assertNotEquals(Integer.parseInt(assignmentIdFromDB), newAssignment);
    }

    @Test
    public static void changeAssignment() {
        Assignment.createNewAssignment();
        System.out.println("\n...changing of assignment...\n");
        String nameOfAssignmentFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForChangeAssignment())
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_ASSIGNMENT + "/" + newAssignment)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("name");
        assertEquals(nameOfAssignmentFromResp, "AutotestAssigmentChanged");
    }
}
