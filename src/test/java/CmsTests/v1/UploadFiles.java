package CmsTests.v1;

import DataBase.queries.ForAtomicObjects;
import Helpers.ListenerForLogs;
import base.BaseTest;
import io.restassured.builder.MultiPartSpecBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.testng.Assert.assertEquals;

@Listeners(ListenerForLogs.class)
public class UploadFiles extends BaseTest {

    private JSONObject getBodyForMediaAtomic(String contentType) {
        Map<String, Object> materialEducationAreasAttributesMap = new HashMap<>();
        materialEducationAreasAttributesMap.put("subject_id", SUBJECT_ID_HARD_CODE);
        materialEducationAreasAttributesMap.put("education_level_id", EDUCATION_LEVEL_ID_HARD_CODE);
        JSONArray materialEducationAreasAttributesJsonArray = new JSONArray();
        materialEducationAreasAttributesJsonArray.put(materialEducationAreasAttributesMap);

        JSONObject body = new JSONObject();
        body.put("material_education_areas_attributes", materialEducationAreasAttributesJsonArray);
        body.put("content_type", contentType);
        body.put("controllable_item_ids", CONTROLLABLE_ITEM_ID_FOR_CMS_ONLY);
        body.put("studying_level_id", STUDYING_LEVEL_ID_HARD_CODE);
        body.put("logical_type_id", LOGICAL_TYPE_ID_HARD_CODE);
        body.put("caption", contentType + "AutoUploadAtomic");
        body.put("folder_ids", ROOT_FOLDER);
        body.put("published", true);
        body.put("time_to_study", "00:11:11");
        return body;
    }

    private void updateInfoForAtomic(String contentType, int atomicObject) {
        JSONObject body = getBodyForMediaAtomic(contentType);
        given()
                .cookies(mapOfCookiesForTeacher)
                .header("Accept", "application/vnd.cms-v1+json")
                .header("Content-Type", JSON)
                .body(body.toString())
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + atomicObject)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public void uploadImageAtomic() throws IOException {
        File imageFile = new File(PATH_TO_JSON_BODIES + "filesForUpload/image.jpg");
        System.out.println("\n...uploading image atomic...\n");
        int atomicObjectIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/vnd.cms-v1+json")
                .headers("Content-Type", "multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(imageFile).fileName("autoUploadFile")
                        .controlName("file")
                        .mimeType("image/jpeg")
                        .build())
                .log().method().log().uri().log().cookies()
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        String atomicObjectFromDb = ForAtomicObjects.getLastAtomicObject();
        assertEquals(atomicObjectIdFromResp, Integer.parseInt(atomicObjectFromDb));

        updateInfoForAtomic("image", atomicObjectIdFromResp);
    }

    @Test
    public void uploadAudioAtomic() throws IOException {
        File audioFile = new File(PATH_TO_JSON_BODIES + "filesForUpload/track.mp3");
        System.out.println("\n...uploading audio atomic...\n");
        int atomicObjectIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/vnd.cms-v1+json")
                .headers("Content-Type", "multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(audioFile).fileName("autoUploadFile")
                        .controlName(audioFile.getName())
                        .mimeType("audio/mp3")
                        .build())
                .log().method().log().uri().log().cookies()
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        String atomicObjectFromDb = ForAtomicObjects.getLastAtomicObject();
        assertEquals(atomicObjectIdFromResp, Integer.parseInt(atomicObjectFromDb));

        updateInfoForAtomic("sound", atomicObjectIdFromResp);
    }

    @Test
    public void uploadVideoAtomic() throws IOException {
        File videoFile = new File(PATH_TO_JSON_BODIES + "filesForUpload/video.mp4");
        System.out.println("\n...uploading video atomic...\n");
        int atomicObjectIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/vnd.cms-v1+json")
                .headers("Content-Type", "multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(videoFile).fileName("autoUploadFile")
                        .controlName(videoFile.getName())
                        .mimeType("video/mp4")
                        .build())
                .log().method().log().uri().log().cookies()
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        String atomicObjectFromDb = ForAtomicObjects.getLastAtomicObject();
        assertEquals(atomicObjectIdFromResp, Integer.parseInt(atomicObjectFromDb));

        updateInfoForAtomic("video", atomicObjectIdFromResp);
    }

    @Test
    public void uploadWordFileAtomic() throws IOException {
        File wordFile = new File(PATH_TO_JSON_BODIES + "filesForUpload/word.docx");
        System.out.println("\n...uploading word file atomic...\n");
        int atomicObjectIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/vnd.cms-v1+json")
                .headers("Content-Type", "multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(wordFile).fileName("autoUploadFile")
                        .controlName(wordFile.getName())
                        .mimeType("application/wps-office.docx")
                        .build())
                .log().method().log().uri().log().cookies()
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        String atomicObjectFromDb = ForAtomicObjects.getLastAtomicObject();
        assertEquals(atomicObjectIdFromResp, Integer.parseInt(atomicObjectFromDb));

        updateInfoForAtomic("file", atomicObjectIdFromResp);
    }
}
