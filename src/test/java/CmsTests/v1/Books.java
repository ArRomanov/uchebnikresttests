package CmsTests.v1;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static DataForTests.PathsAndIds.CMS_BOOKS;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

@Listeners(ListenerForLogs.class)
public class Books extends BaseTest {

    @Test
    public static void getBooks() {
        System.out.println("\n...getting of all books...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_BOOKS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getOneBook() {
        String IdBookVersionFromDB = ForCms.getLastBookVersionId();
        String bookIdFromDB = ForCms.getbookIdByVersionId(IdBookVersionFromDB);
        System.out.println("\n...getting of one books...\n");
        Response response = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_BOOKS + "/" + IdBookVersionFromDB)
                .then().log().body()
                .statusCode(200)
                .extract().response();
        int IdBookVersionFromResp = response.jsonPath().get("id");
        int bookIdFromResp = response.jsonPath().get("book_id");
        Assert.assertEquals(IdBookVersionFromResp, Integer.parseInt(IdBookVersionFromDB));
        Assert.assertEquals(bookIdFromResp, Integer.parseInt(bookIdFromDB));
    }
}
