package CmsTests.v1;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static DataForTests.DataForCms.newAssignmentBookItem;
import static DataForTests.PathsAndIds.CMS_ASSIGNMENT_BOOK_ITEMS;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static ResourcesClasses.JsonBodiesForCms.getBodyForChangeAssignmentBookItem;
import static ResourcesClasses.JsonBodiesForCms.getBodyForNewAssignmentBookItem;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class AssignmentBookItems extends BaseTest {


    @Test
    public static void getAssignmentBook() {
        System.out.println("\n...getting of assignment of book...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_ASSIGNMENT_BOOK_ITEMS)
                .then().log().body()
                .statusCode(200);
    }

    @Test
    public static void getAssignmentBookIdByMethodologist() {
        String assigmentItemIdFromDB = ForCms.getLastAssignmentBook();
        String assigmentIdFromDB = ForCms.getAssignmentIdIdByAssigmentItemId(assigmentItemIdFromDB);
        System.out.println("\n...getting of assignment of book by methodist...\n");
        int assignmentIdFromResp = given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_ASSIGNMENT_BOOK_ITEMS + "/" + assigmentItemIdFromDB)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("assignment_id");
        assertEquals(Integer.parseInt(assigmentIdFromDB), assignmentIdFromResp);
    }

    @Test
    public static void createNewAssignmentItem() {
        System.out.println("\n...creating of assignment item...\n");
        int assignmentItemIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForNewAssignmentBookItem())
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_ASSIGNMENT_BOOK_ITEMS)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        newAssignmentBookItem = assignmentItemIdFromResp;
        String assignmentItemIdFromDB = ForCms.getLastAssignmentBook();
        assertEquals(Integer.parseInt(assignmentItemIdFromDB), assignmentItemIdFromResp);
    }

    @Test
    public static void deleteAssignmentBookItem() {
        AssignmentBookItems.createNewAssignmentItem();
        System.out.println("\n...deleting of assignment item...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .log().method().log().uri().log().cookies().log().body()
                .when().delete(STABLE_BASE_URL + CMS_ASSIGNMENT_BOOK_ITEMS + "/" + newAssignmentBookItem)
                .then().statusCode(204);
        String assignmentBookItemFromDB = ForCms.getLastAssignmentBook();
        assertNotEquals(Integer.parseInt(assignmentBookItemFromDB), newAssignmentBookItem);
    }

    @Test
    public static void changeAssignmentBookItem() {
        AssignmentBookItems.createNewAssignmentItem();
        System.out.println("\n...changing of assignment book item...\n");
        String titleOfAssignmentBookItemFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(getBodyForChangeAssignmentBookItem())
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_ASSIGNMENT_BOOK_ITEMS + "/" + newAssignmentBookItem)
                .then().log().body()
                .statusCode(200)
                .extract().jsonPath().get("title");
        assertEquals(titleOfAssignmentBookItemFromResp, "AutotestItemChanged");
    }
}
