package CmsTests.v3;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForTeacherTests.newBook;
import static DataForTests.PathsAndIds.*;
import static Helpers.clearTestData.OtherMaterials.deleteAllCreatedBooks;
import static ResourcesClasses.JsonBodiesForCms.getBodyForNewBook;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class Books extends BaseTest {

    @Test
    public void getBookVersion() {
        String bookVersionIdFromDB = ForCms.getLastBookVersionId();
        String bookIdFromDB = ForCms.getbookIdByVersionId(bookVersionIdFromDB);
        System.out.println("\n...getting book's version...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_BOOK_VERSION + "/" + bookVersionIdFromDB)
                .then().log().body()
                .statusCode(200)
                .body("id", equalTo(Integer.parseInt(bookVersionIdFromDB)))
                .body("book.id", equalTo(Integer.parseInt(bookIdFromDB)));
    }

    @Test
    public void getBook() {
        String bookVersionIdFromDB = ForCms.getLastBookVersionId();
        String bookIdFromDB = ForCms.getbookIdByVersionId(bookVersionIdFromDB);
        System.out.println("\n...getting of book...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_BOOKS + "/" + bookIdFromDB)
                .then().log().body()
                .statusCode(200)
                .body("id", equalTo(Integer.parseInt(bookIdFromDB)));
    }

    @Test
    public static void createBook() throws SQLException {
        deleteAllCreatedBooks();
        System.out.println("\n...creating of book...\n");
        int bookIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .body(getBodyForNewBook())
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_BOOKS)
                .then().log().body()
                .statusCode(201)
                .body("material_type", equalTo("Book"))
                .body("attributes.ebook_id", equalTo("0AAAAAAA"))
                .extract().jsonPath().get("id");
        newBook = bookIdFromResp;
    }

    @Test
    public static void changeAndDeleteBookAttributes() throws SQLException {
        deleteAllCreatedBooks();
        Books.createBook();
        Map<String, Object> body = new HashMap<>();
        body.put("ebook_id", "0AAAAAAA");
        body.put("title", "changedTitle");
        body.put("authors", "Artem");
        System.out.println("\n...changing of book...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_BOOKS + "/" + newBook)
                .then().log().body()
                .statusCode(200)
                .body("name", equalTo("changedTitle"))
                .body("attributes.authors", equalTo("Artem"));
        System.out.println("\n...deleting of book...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + CMS_BOOKS + "/" + newBook)
                .then().statusCode(204);
        String bookIdFromDBAfterDeletedBook = ForCms.getLastNotDeletedBook();
        Assert.assertNotEquals(Integer.toString(newBook), bookIdFromDBAfterDeletedBook);
    }
}
