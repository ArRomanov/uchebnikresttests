package CmsTests.v3;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForTeacherTests.newComposedDocuments;
import static DataForTests.PathsAndIds.CMS_COMPOSED_DOCUMENT;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class ComposedDocuments extends BaseTest {

    @Test
    public void getComposedDocuments() {
        String lastComposedDocumentFromDb = ForCms.getLastNotDeletedComposedDocument();
        int lastComposedDocumentFromDbToInt = Integer.parseInt(lastComposedDocumentFromDb);
        System.out.println("\n...getting of composed document...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_COMPOSED_DOCUMENT + "/" + lastComposedDocumentFromDb)
                .then().log().body()
                .statusCode(200)
                .assertThat().body("id", equalTo(lastComposedDocumentFromDbToInt));

    }

    @Test
    public void createComposedDocument() throws SQLException {
        String nameOfDoc = "autoCreatedComposedDoc";
        Map<String, Object> body = new HashMap<>();
        body.put("name", nameOfDoc);
        body.put("description", "This document created automatically");
        body.put("json_content_v2", null);
        System.out.println("\n...creating of composed document...\n");
        int idComposedDoc = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_COMPOSED_DOCUMENT)
                .then().log().body()
                .statusCode(201)
                .assertThat()
                .body("material_type", equalTo("ComposedDocument"))
                .body("name", equalTo(nameOfDoc))
                .body("attributes.moderation_status", equalTo("new"))
                .extract().jsonPath().get("id");
        newComposedDocuments = idComposedDoc;
    }

    @Test
    public void deleteComposedDocument() throws SQLException {

    }

    @Test
    public void changeAndDeleteComposedDocuments() throws SQLException {
        createComposedDocument();
        String newNameOfDoc = "autoChangedComposedDoc";
        Map<String, Object> body = new HashMap<>();
        body.put("name", newNameOfDoc);
        body.put("description", "This document changed automatically");
        body.put("json_content_v2", null);
        System.out.println("\n...changing of composed document...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_COMPOSED_DOCUMENT + "/" + newComposedDocuments)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("name", equalTo(newNameOfDoc));
        System.out.println("\n...deleting of composed document...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + CMS_COMPOSED_DOCUMENT + "/" + newComposedDocuments)
                .then().statusCode(204);
        String lastComposedDocFromDB = ForCms.getLastNotDeletedComposedDocument();
        Assert.assertNotEquals(lastComposedDocFromDB, Integer.toString(newComposedDocuments));
    }


}
