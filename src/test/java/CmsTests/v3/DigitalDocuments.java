package CmsTests.v3;

import DataBase.queries.ForCms;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.DataForTeacherTests.newDigitalDocuments;
import static DataForTests.PathsAndIds.CMS_DIGITAL_DOCUMENT;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class DigitalDocuments extends BaseTest {

    @Test
    public void getDigitalDocument() {
        String lastDigitalDocumentFromDb = ForCms.getLastNotDeletedDigitalDocs();
        int lastDigitalDocumentFromDbToInt = Integer.parseInt(lastDigitalDocumentFromDb);
        System.out.println("\n...getting of digital document...\n");
        given()
                .cookies(mapOfCookiesForPublisher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_DIGITAL_DOCUMENT + "/" + lastDigitalDocumentFromDb)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("id", equalTo(lastDigitalDocumentFromDbToInt))
                .body("material_type", equalTo("DigitalDocument"));
    }

    @Test
    public void createDigitalDocument() throws SQLException {
        String nameOfDoc = "autoCreatedDigitalDoc";
        String author = "no name";
        Map<String, Object> body = new HashMap<>();
        body.put("name", nameOfDoc);
        body.put("authors", author);
        body.put("published", false);
        System.out.println("\n...creating of digital document...\n");
        int idDigitalDoc = given()
                .cookies(mapOfCookiesForPublisher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_DIGITAL_DOCUMENT)
                .then().log().body()
                .statusCode(201)
                .assertThat()
                .body("attributes.authors", equalTo(author))
                .body("name", equalTo(nameOfDoc))
                .body("attributes.moderation_status", equalTo("new"))
                .extract().jsonPath().get("id");
        newDigitalDocuments = idDigitalDoc;
    }

    @Test
    public void changeAndDeleteDigitalDocuments() throws SQLException {
        createDigitalDocument();
        String newNameOfDoc = "autoChangedDigitalDoc";
        String newAuthor = "yes name";
        Map<String, Object> body = new HashMap<>();
        body.put("name", newNameOfDoc);
        body.put("authors", newAuthor);
        System.out.println("\n...changing of digital document...\n");
        given()
                .cookies(mapOfCookiesForPublisher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + CMS_DIGITAL_DOCUMENT + "/" + newDigitalDocuments)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("name", equalTo(newNameOfDoc))
                .body("attributes.authors", equalTo(newAuthor));
        System.out.println("\n...deleting of digital document...\n");
        given()
                .cookies(mapOfCookiesForPublisher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().delete(STABLE_BASE_URL + CMS_DIGITAL_DOCUMENT + "/" + newDigitalDocuments)
                .then().statusCode(204);
        String lastComposedDocFromDB = ForCms.getLastNotDeletedDigitalDocs();
        Assert.assertNotEquals(lastComposedDocFromDB, Integer.toString(newDigitalDocuments));
    }
}
