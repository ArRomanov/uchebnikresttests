package CmsTests.v3;

import DataBase.queries.ForTestSpecs;
import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static DataForTests.PathsAndIds.TEST_SPECIFICATIONS;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;

@Listeners(ListenerForLogs.class)
public class TestSpecifications extends BaseTest {

    @Test
    public static void getTestSpec() {
        String idTestSpec = ForTestSpecs.getLastTestSpec();
        int idTestSpecInt = Integer.parseInt(idTestSpec);
        System.out.println("\n...getting all test specifications...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + TEST_SPECIFICATIONS + "/" + idTestSpecInt)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("id", equalTo(idTestSpecInt))
                .body("material_type", equalTo("TestSpecification"));
    }

}
