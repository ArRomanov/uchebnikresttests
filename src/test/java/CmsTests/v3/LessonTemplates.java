package CmsTests.v3;

import Helpers.ListenerForLogs;
import base.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataBase.queries.ForLessons.getModerationStatusOfLesson;
import static DataForTests.DataForMethodologistTests.ACCEPTED_MODERATION_STATUS;
import static DataForTests.DataForMethodologistTests.REJECTED_MODERATION_STATUS;
import static DataForTests.DataForTeacherTests.newLessonTemplate;
import static DataForTests.PathsAndIds.LESSON_TEMPLATES;
import static DataForTests.PathsAndIds.STABLE_BASE_URL;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class LessonTemplates extends BaseTest {

    @Test
    public static void getLessonTemplate() {
        newLessonTemplate = lessonFactory.createNew();
        System.out.println("\n...getting of lesson template...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + newLessonTemplate)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("id", equalTo(newLessonTemplate))
                .body("attributes.moderation_status", equalTo("new"));
    }

    @Test
    public static void createAndDeleteLesson() throws SQLException {
        newLessonTemplate = lessonFactory.createNew();
        lessonFactory.deleteObj(newLessonTemplate);
        int otherLesson = lessonFactory.getLastFromDb();
        assertNotEquals(newLessonTemplate, otherLesson);
    }

    @Test
    public static void changeLessonTemplate() {
        newLessonTemplate = lessonFactory.createNew();
        Map<String, String> body = new HashMap<>();
        body.put("name", "New name");
        body.put("topic_name", "New topic");
        System.out.println("\n...changing of lesson template...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .header("Accept", "application/vnd.cms-v3+json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + newLessonTemplate)
                .then().log().body()
                .statusCode(200)
                .assertThat()
                .body("name", equalTo("New name"))
                .body("attributes.topic_name", equalTo("New topic"));
    }

    @Test
    public static void moderationOfLesson() {
        newLessonTemplate = lessonFactory.createNew();
        lessonFactory.toModerationStatus(newLessonTemplate);
        String statusOfModerationFromDB = getModerationStatusOfLesson(newLessonTemplate);
        assertEquals(statusOfModerationFromDB, ACCEPTED_MODERATION_STATUS);
    }


    @Test
    public static void rejectModeration() {
        newLessonTemplate = lessonFactory.createNew();
        lessonFactory.toModerationStatus(newLessonTemplate);
        Map<String, Object> body = new HashMap<>();
        body.put("published", true);
        body.put("moderation_status", "rejected");
        body.put("reject_reason", "так-то!");
        System.out.println("\n...rejecting lesson template by moderator...\n");
        given()
                .cookies(mapOfCookiesForMethodist)
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(body)
                .log().method().log().uri().log().cookies().log().body()
                .when().patch(STABLE_BASE_URL + LESSON_TEMPLATES + "/" + newLessonTemplate)
                .then().statusCode(200);
        String statusOfModerationFromDB = getModerationStatusOfLesson(newLessonTemplate);
        assertEquals(statusOfModerationFromDB, REJECTED_MODERATION_STATUS);
    }
}
