package CmsTests.v3;

import DataBase.queries.ForAtomicObjects;
import Helpers.ListenerForLogs;
import base.BaseTest;
import io.restassured.builder.MultiPartSpecBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static DataForTests.PathsAndIds.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Listeners(ListenerForLogs.class)
public class AtomicObjects extends BaseTest {

    private JSONObject getBodyForMediaAtomic(String contentType) {
        Map<String, Object> materialEducationAreasAttributesMap = new HashMap<>();
        materialEducationAreasAttributesMap.put("subject_id", SUBJECT_ID_HARD_CODE);
        materialEducationAreasAttributesMap.put("education_level_id", EDUCATION_LEVEL_ID_HARD_CODE);
        JSONArray materialEducationAreasAttributesJsonArray = new JSONArray();
        materialEducationAreasAttributesJsonArray.put(materialEducationAreasAttributesMap);

        JSONObject body = new JSONObject();
        body.put("material_education_areas_attributes", materialEducationAreasAttributesJsonArray);
        body.put("content_type", contentType);
        body.put("controllable_item_ids", CONTROLLABLE_ITEM_ID_FOR_CMS_ONLY);
        body.put("studying_level_id", STUDYING_LEVEL_ID_HARD_CODE);
        body.put("logical_type_id", LOGICAL_TYPE_ID_HARD_CODE);
        body.put("caption", contentType + "AutoUploadAtomic");
        body.put("folder_ids", ROOT_FOLDER);
        body.put("published", true);
        body.put("time_to_study", "00:11:11");
        return body;
    }

    private int createAtomic(String contentType) {
        JSONObject body = getBodyForMediaAtomic(contentType);
        int atomicId = given()
                .cookies(mapOfCookiesForTeacher)
                .header("Accept", "application/vnd.cms-v1+json")
                .header("Content-Type", JSON)
                .body(body.toString())
                .log().method().log().uri().log().cookies().log().body()
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().log().body()
                .statusCode(201)
                .extract().jsonPath().get("id");
        return atomicId;
    }

    @Test
    public void createAndPatchAtomicImage() throws IOException {
        int atomicId = createAtomic("image");
        File imageFile = new File(PATH_TO_JSON_BODIES + "filesForUpload/image.jpg");
        System.out.println("\n...creating image atomic...\n");
        given()
                .cookies(mapOfCookiesForTeacher)
                .headers("Accept", "application/vnd.cms-v3+json")
                .headers("Content-Type", "multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(imageFile).fileName("autoUploadFile")
                        .controlName("file")
                        .mimeType("image/jpeg")
                        .build())
                .log().method().log().uri().log().cookies()
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + atomicId)
                .then().log().body()
                .statusCode(200);
        String atomicObjectFromDb = ForAtomicObjects.getLastAtomicObject();
        assertEquals(atomicId, Integer.parseInt(atomicObjectFromDb));
    }

    @Test
    public void getAndDeleteAtomicObj() throws SQLException {
        int newAtomic = atomicFactory.createNew();
        System.out.println("\n...getting atomic info...\n");
        int atomicIdFromResp = given()
                .cookies(mapOfCookiesForTeacher)
                .contentType(JSON)
                .headers("Accept", "application/vnd.cms-v3+json")
                .log().method().log().uri().log().cookies()
                .when().get(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + newAtomic)
                .then().log().body()
                .statusCode(200)
                .extract().response().jsonPath().get("id");
        assertEquals(atomicIdFromResp, newAtomic);
        atomicFactory.deleteObj(newAtomic);
        int lastNotDeletedAtomic = atomicFactory.getLastFromDb();
        assertNotEquals(lastNotDeletedAtomic, newAtomic);
    }
}
